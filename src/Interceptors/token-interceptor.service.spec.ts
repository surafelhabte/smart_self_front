/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { TokenInterceptorService } from './token-interceptor.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

describe('Service: TokenInterceptor', () => {
  let tokenInterceptService: TokenInterceptorService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule]
    });

    tokenInterceptService = TestBed.get(TokenInterceptorService);
  });

  it('should be created', () => {
    const service: TokenInterceptorService = new TokenInterceptorService();
    expect(service).toBeTruthy();
  });
});
