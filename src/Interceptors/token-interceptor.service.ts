import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppUserAuth } from 'src/core/security.service';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService {
  public securityObject: AppUserAuth;
  constructor() {
    this.securityObject = new AppUserAuth();
    if (localStorage.getItem('selfServiceAccessToken')) {
      this.securityObject = JSON.parse(
        localStorage.getItem('selfServiceAccessToken')
      );
    }
  }
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (this.securityObject.key) {
      const cloned = req.clone({
        headers: req.headers.set('Authorization', this.securityObject.key)
      });
      return next.handle(cloned);
    } else {
      const cloned = req.clone({
        headers: req.headers.set('Authorization', '')
      });
      return next.handle(cloned);
    }
  }
}
