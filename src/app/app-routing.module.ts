import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () =>
      import('../featured/auth-management/auth-management.module').then(
        m => m.AuthManagementModule
      )
  },
  {
    path: '',
    loadChildren: () =>
      import('../featured/featured.module').then(m => m.FeaturedModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
