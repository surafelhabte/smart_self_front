import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthService } from 'src/featured/auth-management/services/auth.service';

export class AppUserAuth {
  full_name: '';
  employee_id: '';
  position_id: '';
  position: '';
  key: '';
  isAuthenticated = false;
  roles: RoleModel[] = [];
}

export interface RoleModel {
  role: string;
  status: string;
}

export class AppUser {
  userName = '';
  password = '';
}

@Injectable({
  providedIn: 'root'
})
export class SecurityService {
  securityObject: AppUserAuth;

  logIn(userName, password): Observable<AppUserAuth> {
    if (!this.securityObject.isAuthenticated && true) {
      this.authService.login(userName, password).subscribe((data: any) => {
        if (data.status) {
          this.securityObject = data.message;
          localStorage.setItem(
            'selfServiceAccessToken',
            JSON.stringify(this.securityObject)
          );
          window.location.replace('/workspace');
        } else {
          alert('Incorrect username or password');
        }
      });
    }
    return of<AppUserAuth>(this.securityObject);
  }
  
  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private authService: AuthService
  ) {
    this.securityObject = new AppUserAuth();
    if (localStorage.getItem('selfServiceAccessToken')) {
      this.securityObject = JSON.parse(
        localStorage.getItem('selfServiceAccessToken')
      );
    }
  }

  resetSecurityObject(): void {
    this.securityObject.full_name = '';
    this.securityObject.employee_id = '';
    this.securityObject.position_id = '';
    this.securityObject.position = '';
    this.securityObject.key = '';
    this.securityObject.isAuthenticated = false;
    this.securityObject.roles = [];
    localStorage.removeItem('selfServiceAccessToken');
    localStorage.removeItem('range');
  }

  logOut() {
    this.resetSecurityObject();
    window.location.reload();
  }

  hasClaim(role: any, status?: any): boolean {
    let ret = false;
    if (typeof role === 'string') {
      ret = this.isClaimValid(role, status);
    } else {
      const claims: string[] = role;

      if (claims) {
        for (let index = 0; index < claims.length; index++) {
          ret = this.isClaimValid(claims[index]);
          if (ret) {
            break;
          }
        }
      }
    }
    return ret;
  }

  private isClaimValid(role: string, status?: string): boolean {
    let ret = false;
    let auth: AppUserAuth = null;

    auth = this.securityObject;

    if (auth) {
      if (role.indexOf(':') >= 0) {
        const words: string[] = role.split(':');
        role = words[0].toLocaleLowerCase();
        status = words[1];
      } else {
        role = role.toLocaleLowerCase();
        status = status ? status : 'true';
      }

      const s = auth.roles.find(
        c => c.role.toLocaleLowerCase() === role && c.status === status
      );

      ret =
        auth.roles.find(
          c => c.role.toLocaleLowerCase() === role && c.status === status
        ) != null;
    }

    return ret;
  }
}
