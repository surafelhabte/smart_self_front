import { Injectable } from '@angular/core';
import {
  CanActivate,
  Router,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  UrlTree
} from '@angular/router';
import { Observable } from 'rxjs';
import { SecurityService } from './security.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  constructor(
    private router: Router,
    private securityService: SecurityService
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const role: string = next.data['role'];
    if (!this.securityService.securityObject.isAuthenticated) {
      this.router.navigate(['login']);
    } else {
      if (this.securityService.hasClaim(role)) {
        return true;
      } else {
        this.router.navigate(['unauthorized']);
      }
      return true;
    }
    return true;
  }

  canLoad(): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.securityService.securityObject.isAuthenticated) {
      this.router.navigate(['login']);
      return false;
    } else {
      return true;
    }
  }
}
