/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SecurityService } from './security.service';
import {
  HttpTestingController,
  HttpClientTestingModule
} from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from 'src/shared/shared.module';

describe('Service: Security service ', () => {
  let mService: SecurityService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule, SharedModule]
    });

    mService = TestBed.get(SecurityService);
    httpMock = TestBed.get(HttpTestingController);
  });
  it('should ...', inject([SecurityService], (service: SecurityService) => {
    expect(service).toBeTruthy();
  }));
});
