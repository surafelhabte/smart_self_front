import {
  TestBed,
  async,
  ComponentFixture,
  TestModuleMetadata
} from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from 'src/shared/shared.module';
import { FormOptionComponent } from './form-option.component';
import { DebugElement } from '@angular/core';

describe('FormOptionComponent', () => {
  let component: FormOptionComponent;
  let fixture: ComponentFixture<FormOptionComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [RouterTestingModule],
      declarations: [FormOptionComponent]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(FormOptionComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };
  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create ', () => {
    const fixture = TestBed.createComponent(FormOptionComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
