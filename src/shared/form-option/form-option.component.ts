import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-form-option',
  templateUrl: './form-option.component.html',
  styleUrls: ['./form-option.component.css']
})
export class FormOptionComponent implements OnInit {
  @Input()
  public submitDisabled: boolean;
  @Input()
  public cancelDisabled: boolean;
  @Input()
  public isUpdate: boolean;
  @Input()
  public buttonText: string;
  constructor(private location: Location) {}

  ngOnInit() {
    if (this.isUpdate) {
      this.buttonText = 'Update';
    } else { this.buttonText = 'Save'; }
  }
  cancel() {
    this.location.back();
  }
}
