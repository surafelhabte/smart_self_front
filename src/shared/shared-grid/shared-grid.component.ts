import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild
} from '@angular/core';
import {
  GridModel,
  PageSettingsModel,
  SearchSettingsModel,
  TextWrapSettingsModel,
  EditSettingsModel,
  CommandModel,
  GridComponent,
  IRow,
  Column
} from '@syncfusion/ej2-angular-grids';
import { EmitType, closest } from '@syncfusion/ej2-base';
import { ClickEventArgs } from '@syncfusion/ej2-angular-navigations';
import { SecurityService } from 'src/core/security.service';

@Component({
  selector: 'app-shared-grid',
  templateUrl: './shared-grid.component.html',
  styleUrls: ['./shared-grid.component.css']
})
export class SharedGridComponent implements OnInit {
  @Input() gridData: any;
  @Input() allowOperation: any;
  @Input() allowToolbar: any;
  @Input() searchField: any;
  @Input() incomingCommand: any;
  @Input() columns: any[] = [];
  @Input() childGrid: GridModel;
  @Input() grouped: boolean;
  @Input() groupOptions: any;
  @Input() height: any;
  @Input() showColumnChooser: boolean;
  @Input() allowExporttoExcel: boolean;
  @Input() addPrivilage: string;
  @Input() editPrivilage: string;
  @Input() deletePrivilage: string;
  @Input() showAdd = false;
  @Input() showExcelExport = false;
  @Input() showPrint = false;
  @Input() showSearch = false;
  @Input() showFilter = false;
  @Input() showCollapseAndExpand = false;
  @Input() incomingToolbar: any;

  @Output() public addRecord: EventEmitter<any> = new EventEmitter();
  @Output() public editRecord: EventEmitter<any> = new EventEmitter();
  @Output() public deleteRecord: EventEmitter<any> = new EventEmitter();

  public currentDeletingItem: any;
  public pageSettings: PageSettingsModel;
  public toolbar: Array<any>;
  public searchOptions: SearchSettingsModel;

  public wrapSettings: TextWrapSettingsModel;
  public editSettings: EditSettingsModel;
  public commands: CommandModel[];
  public onfilter = false;

  @ViewChild('grid')
  public grid: GridComponent;
  public pageSizes = ['10', '50', '100', '200', 'All'];
  constructor(private securityService: SecurityService) {}

  ngOnInit() {
    this.pageSettings = { pageSize: 10, pageSizes: this.pageSizes };
    this.wrapSettings = { wrapMode: 'Content' };
    this.toolbar = this.incomingToolbar;

    this.searchOptions = {
      fields: [this.searchField],
      operator: 'contains',
      key: '',
      ignoreCase: true
    };

    this.editSettings = { allowAdding: true };
    if (this.incomingCommand) {
      if (this.editPrivilage) {
        if (
          this.incomingCommand.edit === true &&
          this.securityService.hasClaim(this.editPrivilage)
        ) {
          this.commands = [
            {
              type: 'Edit',
              buttonOption: {
                cssClass: 'e-flat',
                iconCss: 'e-edit e-icons',
                click: this.onEdit.bind(this)
              }
            }
          ];
        }
      } else {
        if (this.incomingCommand.edit === true) {
          this.commands = [
            {
              type: 'Edit',
              buttonOption: {
                cssClass: 'e-flat',
                iconCss: 'e-edit e-icons',
                click: this.onEdit.bind(this)
              }
            }
          ];
        }
      }

      if (this.deletePrivilage) {
        if (
          this.incomingCommand.delete === true &&
          this.securityService.hasClaim(this.deletePrivilage)
        ) {
          this.commands = [
            {
              type: 'Delete',
              buttonOption: {
                cssClass: 'e-flat',
                iconCss: 'e-delete e-icons',
                click: this.onDelete.bind(this)
              }
            }
          ];
        }
      } else {
        if (this.incomingCommand.delete === true) {
          this.commands = [
            {
              type: 'Delete',
              buttonOption: {
                cssClass: 'e-flat',
                iconCss: 'e-delete e-icons',
                click: this.onDelete.bind(this)
              }
            }
          ];
        }
      }

      if (this.editPrivilage && this.deletePrivilage) {
        if (
          this.incomingCommand.delete === true &&
          this.incomingCommand.edit === true &&
          this.securityService.hasClaim(this.editPrivilage) &&
          this.securityService.hasClaim(this.deletePrivilage)
        ) {
          this.commands = [
            {
              type: 'Edit',
              buttonOption: {
                cssClass: 'e-flat',
                iconCss: 'e-edit e-icons',
                click: this.onEdit.bind(this)
              }
            },
            {
              type: 'Delete',
              buttonOption: {
                cssClass: 'e-flat',
                iconCss: 'e-delete e-icons',
                click: this.onDelete.bind(this)
              }
            }
          ];
        }
      } else {
        if (
          this.incomingCommand.delete === true &&
          this.incomingCommand.edit === true
        ) {
          this.commands = [
            {
              type: 'Edit',
              buttonOption: {
                cssClass: 'e-flat',
                iconCss: 'e-edit e-icons',
                click: this.onEdit.bind(this)
              }
            },
            {
              type: 'Delete',
              buttonOption: {
                cssClass: 'e-flat',
                iconCss: 'e-delete e-icons',
                click: this.onDelete.bind(this)
              }
            }
          ];
        }
      }

      if (
        this.incomingCommand.delete !== true &&
        this.incomingCommand.edit !== true
      ) {
        this.commands = [];
      }
    }
    this.initializeToolBar();
  }

  initializeToolBar(): void {
    this.toolbar = [];
    if (this.addPrivilage) {
      if (this.showAdd && this.securityService.hasClaim(this.addPrivilage)) {
        this.toolbar.push({
          text: 'Add',
          prefixIcon: 'e-create'
        });
      }
    } else {
      if (this.showAdd) {
        this.toolbar.push({
          text: 'Add',
          prefixIcon: 'e-create'
        });
      }
    }

    if (this.showFilter) {
      this.toolbar.push({
        text: 'Filter',
        prefixIcon: 'e-BT_Excelfilter'
      });
    }
    if (this.showExcelExport) {
      this.toolbar.push({
        text: 'ExcelExport',
        prefixIcon: 'e-Excel_Export'
      });
    }
    if (this.showPrint) {
      this.toolbar.push({
        text: 'Print',
        prefixIcon: 'e-print'
      });
    }

    if (this.showSearch) {
      this.toolbar.push({
        text: 'Search',
        tooltipText: 'Search Items'
      });
    }
    if (this.showCollapseAndExpand) {
      this.toolbar.push(
        {
          text: 'Expand',
          prefixIcon: 'e-expand',
          tooltipText: 'Expand all'
        },
        {
          text: 'Collapse',
          prefixIcon: 'e-collapse',
          tooltipText: 'Collapse all'
        }
      );
    }
  }

  onEdit(args: Event): void {
    const rowObj: IRow<Column> = this.grid.getRowObjectFromUID(
      closest(args.target as Element, '.e-row').getAttribute('data-uid')
    );
    const data1: any = rowObj.data;
    this.editRecord.emit(data1);
  }

  onDelete(args: Event): void {
    const rowObj: IRow<Column> = this.grid.getRowObjectFromUID(
      closest(args.target as Element, '.e-row').getAttribute('data-uid')
    );
    const data1: any = rowObj.data;
    this.deleteRecord.emit(data1);
    this.currentDeletingItem = data1;
  }

  toolbarClicked(args: ClickEventArgs): void {
    switch (args.item.text) {
      case 'Add':
        this.addRecord.emit(args.item.id);
        break;
      case 'Filter':
        if (this.onfilter) {
          this.grid.allowFiltering = false;
          this.onfilter = !this.onfilter;
        } else {
          this.grid.allowFiltering = true;
          this.onfilter = !this.onfilter;
        }
        break;
      case 'Collapse':
        this.grid.groupModule.collapseAll();
        break;
      case 'Expand':
        this.grid.groupModule.expandAll();
        break;
      case 'ExportExcel':
        this.grid.excelExport();
        break;
      case 'Print':
        this.grid.print();
        break;
    }
  }
}
