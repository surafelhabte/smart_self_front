import { SharedGridComponent } from './shared-grid.component';
import {
  ComponentFixture,
  TestModuleMetadata,
  TestBed,
  async
} from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { GridModule } from '@syncfusion/ej2-angular-grids';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('SharedGridComponent', () => {
  let component: SharedGridComponent;
  let fixture: ComponentFixture<SharedGridComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [RouterTestingModule, GridModule, HttpClientTestingModule],
      declarations: [SharedGridComponent]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(SharedGridComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };
  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create ', () => {
    const fixture = TestBed.createComponent(SharedGridComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
