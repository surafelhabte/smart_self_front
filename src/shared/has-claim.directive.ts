import { Directive, TemplateRef, ViewContainerRef, Input } from '@angular/core';
import { SecurityService } from 'src/core/security.service';

@Directive({
  selector: '[appHasClaim]'
})
export class HasClaimDirective {
  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private securityService: SecurityService
  ) {}

  @Input() set hasClaim(claimType: any) {
    if (this.securityService.hasClaim(claimType)) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainer.clear();
    }
  }
}
