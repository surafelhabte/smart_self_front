import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { EmployeeService } from '../service/employee.service';
import { SecurityService } from 'src/core/security.service';
import { Query } from '@syncfusion/ej2-data/src';

@Component({
  selector: 'app-request-for-other',
  templateUrl: './request-for-other.component.html',
  styleUrls: ['./request-for-other.component.css']
})
export class RequestForOtherComponent implements OnInit {
  [x: string]: any;
  autoItem: any;
  public employeeReqId: null;
  @Output() parentEvent: EventEmitter<any> = new EventEmitter();
  @Input() isUpdate = false;
  checked = false;
  public requestFields: object = { text: 'full_name', value: 'employee_id' };
  requestEmployees: [];
  requestForOtherPrivillege = 'requestForOther';
  canRequestForOther = false;
  constructor(
    private empService: EmployeeService,
    public securityService: SecurityService
  ) {
    if (localStorage.getItem('selfServiceAccessToken')) {
      this.securityObject = JSON.parse(
        localStorage.getItem('selfServiceAccessToken')
      );
    }
  }

  ngOnInit() {
    this.autoItem = document.getElementsByClassName(
      'autocomplete'
    ) as HTMLCollectionOf<HTMLElement>;
    this.canRequestForOther = this.securityService.hasClaim(
      this.requestForOtherPrivillege
    );
  }

  onFilteringRequestFor(e: any) {
    let query = new Query();
    query =
      e.text != '' ? query.where('full_name', 'contains', e.text, true) : query;
    const filterInfo = {
      keyword: e.text
    };
    this.canRequestForOther
      ? this.empService
          .getRequestEmployees(filterInfo)
          .subscribe((data: any) => {
            e.updateData(data);
          })
      : null;
  }

  requestForOther() {
    this.checked = !this.checked;
    this.checked
      ? (this.autoItem[0].style.display = 'block')
      : (this.autoItem[0].style.display = 'none') &&
        this.parentEvent.emit(this.securityObject.employee_id);
    // this.checked ? this.getRequestEmployees() : null;
  }

  onrequestEmployeeChange(event) {
    this.employeeReqId = event.itemData.employee_id;
    this.parentEvent.emit(this.employeeReqId);
  }

  onEmployeeChange() {}
}
