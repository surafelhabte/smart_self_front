import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  private url = environment.baseUrl;

  constructor(private http: HttpClient) {}

  getRequestEmployees(filterInfo) {
    return this.http.post(this.url + 'employee/employee/gets', filterInfo);
  }
}
