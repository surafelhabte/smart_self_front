import { Component, ViewChild } from '@angular/core';
import { ToastComponent } from '@syncfusion/ej2-angular-notifications';

@Component({
  selector: 'app-shared-toast',
  templateUrl: './shared-toast.component.html',
  styleUrls: ['./shared-toast.component.css']
})
export class SharedToastComponent {
  @ViewChild('element') element : ToastComponent;

  public position = { X: 'Center', Y: 'Top' };
  public data: any = undefined;

  Show(data: any) {
    this.element.show(data);
    history.state.data = undefined;
  }

  onClick(e) {
    e.clickToClose = true;
  }
}
