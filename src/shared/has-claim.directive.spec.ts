/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { HasClaimDirective } from './has-claim.directive';
import { TemplateRef, ViewContainerRef } from '@angular/core';
import { SecurityService } from 'src/core/security.service';

describe('Directive: HasClaim', () => {
  let templateRef: TemplateRef<any>;
  let viewContainerRef: ViewContainerRef;
  let secService: SecurityService;
  it('should create an instance', () => {
    const directive = new HasClaimDirective(
      templateRef,
      viewContainerRef,
      secService
    );
    expect(directive).toBeTruthy();
  });
});
