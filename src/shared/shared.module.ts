import { NgModule, Injectable } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  GridModule,
  DetailRowService,
  PageService,
  SortService,
  FilterService,
  GroupService,
  ReorderService,
  ResizeService,
  ToolbarService,
  SearchService,
  CommandColumnService,
  EditService,
  ColumnChooserService,
  ExcelExportService
} from '@syncfusion/ej2-angular-grids';
import { TextBoxModule } from '@syncfusion/ej2-angular-inputs';
import {
  DateTimePickerModule,
  DatePickerModule
} from '@syncfusion/ej2-angular-calendars';

import {
  SidebarModule,
  TreeViewModule,
  AccordionModule,
  TabModule
} from '@syncfusion/ej2-angular-navigations';
import { DialogModule } from '@syncfusion/ej2-angular-popups';
import { ButtonModule } from '@syncfusion/ej2-angular-buttons';
import { SharedGridComponent } from './shared-grid/shared-grid.component';
import { ToastModule } from '@syncfusion/ej2-angular-notifications';
import { SharedToastComponent } from './shared-toast/shared-toast.component';
import { TitleViewComponent } from './title-view/title-view.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { TokenInterceptorService } from 'src/Interceptors/token-interceptor.service';
import { DropDownListModule } from '@syncfusion/ej2-angular-dropdowns';
import { NumericTextBoxModule } from '@syncfusion/ej2-angular-inputs';
import { FormOptionComponent } from './form-option/form-option.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DropDownButtonModule } from '@syncfusion/ej2-angular-splitbuttons';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CheckBoxModule } from '@syncfusion/ej2-angular-buttons';
import { AutoCompleteModule } from '@syncfusion/ej2-angular-dropdowns';
import { RequestForOtherComponent } from './request-for-other/request-for-other.component';

@Injectable()
@NgModule({
  imports: [
    CommonModule,
    DateTimePickerModule,
    DatePickerModule,
    GridModule,
    ToastModule,
    NgbModule,
    CheckBoxModule,
    AutoCompleteModule,
    DialogModule,
    TextBoxModule,
    DropDownListModule,
    NumericTextBoxModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    TabModule
  ],
  declarations: [
    SharedGridComponent,
    SharedToastComponent,
    FormOptionComponent,
    TitleViewComponent,
    RequestForOtherComponent
  ],
  exports: [
    TreeViewModule,
    GridModule,
    SidebarModule,
    SharedToastComponent,
    SharedGridComponent,
    TitleViewComponent,
    TextBoxModule,
    DialogModule,
    ButtonModule,
    DropDownListModule,
    NumericTextBoxModule,
    FormOptionComponent,
    TreeViewModule,
    DateTimePickerModule,
    DatePickerModule,
    AccordionModule,
    DropDownButtonModule,
    NgbModule,
    CheckBoxModule,
    AutoCompleteModule,
    RequestForOtherComponent,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
    TabModule
    ],
  providers: [
    DetailRowService,
    PageService,
    SortService,
    FilterService,
    GroupService,
    ReorderService,
    ResizeService,
    ToolbarService,
    SearchService,
    CommandColumnService,
    EditService,
    ColumnChooserService,
    ExcelExportService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ]
})
export class SharedModule {}
