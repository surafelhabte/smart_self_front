import {
  ComponentFixture,
  TestModuleMetadata,
  TestBed,
  async
} from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from 'src/shared/shared.module';
import { TerminationListComponent } from './termination-list.component';
import { TerminationService } from '../termination.service';

describe('TerminationListComponent', () => {
  let component: TerminationListComponent;
  let fixture: ComponentFixture<TerminationListComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [RouterTestingModule, HttpClientTestingModule, SharedModule],
      providers: [TerminationService],
      declarations: [TerminationListComponent]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(TerminationListComponent);
    component = fixture.componentInstance;
    component.securityObject = {
      username: 'naol'
    };
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
