import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';
import { TerminationService } from '../termination.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-termination-list',
  templateUrl: './termination-list.component.html',
  styleUrls: ['./termination-list.component.css']
})
export class TerminationListComponent implements OnInit {
  @ViewChild('notif') notif: SharedToastComponent;

  public data: any = [];
  public columnsToDisplay: any[] = [];
  public incomingCommand = { edit: true, delete: true };
  public id: any;
  securityObject: any;

  constructor(
    private service: TerminationService,
    private router: Router,
    private actRoute: ActivatedRoute
  ) {
    this.prepare();

    if (localStorage.getItem('selfServiceAccessToken')) {
      this.securityObject = JSON.parse(
        localStorage.getItem('selfServiceAccessToken')
      );
    }
  }

  ngOnInit() {
    this.service
      .filter({ emp_id: this.securityObject.employee_id, action_id: '6' })
      .subscribe((data: any) => {
        this.data = data;
      });
  }

  add() {
    this.router.navigate(['workspace/termination/create']);
  }

  edit($event) {
    this.router.navigate(['workspace/termination/' + $event.id + '/update']);
  }

  delete($event) {
    const conf = confirm('Are you sure you want to delete this request ?');
    if (conf) {
      this.service.delete($event.id).subscribe((response: any) => {
        if (response.status === true) {
          this.notif.Show({
            title: 'Success',
            content: response.message,
            cssClass: 'e-toast-success'
          });
          this.ngOnInit();
        } else {
          this.notif.Show({
            title: 'Error',
            content: response.message,
            cssClass: 'e-toast-danger'
          });
        }
      });
    }
  }

  prepare() {
    const col = [
      {
        field: 'name',
        headerText: 'Name'
      },
      {
        field: 'status',
        headerText: 'Status'
      },
      {
        field: 'created_date',
        headerText: 'create Date'
      }
    ];
    col.forEach(element => {
      this.columnsToDisplay.push({
        field: element.field,
        headerText: element.headerText,
        textAlign: 'center',
        width: 120
      });
    });
  }
}
