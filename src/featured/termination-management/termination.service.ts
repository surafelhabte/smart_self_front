import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TerminationService {
  private url = environment.baseUrl + 'termination/termination';

  constructor(private http: HttpClient) {}

  create(request: any): any {
    return this.http.post(this.url + '/create', request);
  }

  filter(filter) {
    return this.http.post(this.url + '/filter', filter);
  }

  get(id: any) {
    return this.http.get(this.url + '/get/' + id);
  }

  update(request: any) {
    return this.http.post(this.url + '/update', request);
  }

  delete(id: any) {
    return this.http.delete(this.url + '/delete/' + id);
  }
}
