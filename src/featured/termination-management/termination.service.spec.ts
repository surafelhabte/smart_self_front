import { TestBed } from '@angular/core/testing';
import {
  HttpTestingController,
  HttpClientTestingModule
} from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TerminationService } from './termination.service';

describe('Service: Termination', () => {
  let mService: TerminationService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule]
    });

    mService = TestBed.get(TerminationService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('Should get and list filtered requests ', () => {
    const workflow = [
      {
        id: '29ea152d-53c5-11ea-857b-8cec4ba694b1',
        name: 'Medical Refund',
        status: null,
        step: '1',
        created_date: '2020-02-20 12:41:32'
      },
      {
        id: '29ea152d-53c5-11ea-857b-8cec4ba694b1',
        name: 'Medical Refund',
        status: null,
        step: '1',
        created_date: '2020-02-20 12:41:32'
      }
    ];

    mService.get({ action_id: 2, emp_id: 'EP123' }).subscribe((data: any) => {
      expect(data).toBe(workflow);
    });
    // telling the httmock what kind of request we expect and toward which url
    const req = httpMock.expectOne(() => {
      return true;
    });
    expect(req.request.method).toBe('GET');

    // fire the request with its data we really expect

    req.flush(workflow);

    httpMock.verify();
  });
  it('Should create leave request', () => {
    const activity = {
      action_id: '1',
      workflow: [
        { step: 1, authorized_by: 'Manager' },
        { step: 2, authorized_by: 'Officers' }
      ]
    };
    mService.create(activity).subscribe((data: any) => {
      expect(data.workflow.length).toBeGreaterThan(0);
    });
    const req = httpMock.expectOne(() => {
      return true;
    });
    expect(req.request.method).toBe('POST');
    req.flush(activity);
    httpMock.verify();
  });

  it('Should get leave request successfull', () => {
    const activity = {
      action_id: '2',
      workflow: [
        { step: 1, authorized_by: 'Manager' },
        { step: 2, authorized_by: 'Officers' }
      ]
    };

    mService.get(activity.action_id).subscribe((data: any) => {
      expect(data).toBe(activity);
    });
    // telling the httmock what kind of request we expect and toward which url
    const req = httpMock.expectOne(() => {
      return true;
    });
    expect(req.request.method).toBe('GET');

    // fire the request with its data we really expect

    req.flush(activity);

    httpMock.verify();
  });

  it('Should update leave request', () => {
    const activity = {
      action_id: '1',
      workflow: [
        { step: 1, authorized_by: 'Manager' },
        { step: 2, authorized_by: 'Officers' }
      ]
    };

    mService.update(activity).subscribe((data: any) => {
      expect(data.activity.action).toBe('1');
    });
    const req = httpMock.expectOne(() => {
      return true;
    });
    expect(req.request.method).toBe('POST');

    req.flush(activity);

    httpMock.verify();
  });

  it('Should delete termination request', () => {
    mService.delete(12).subscribe((data: any) => {
      expect(data).toBe(12);
    });

    const req = httpMock.expectOne(() => {
      return true;
    });
    expect(req.request.method).toBe('DELETE');

    req.flush(12);

    httpMock.verify();
  });
});
