import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TerminationManagementComponent } from './termination-form/termination-management.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/shared/shared.module';
import { TerminationListComponent } from './termination-list/termination-list.component';

const routes: Routes = [
  {
    path: 'create',
    component: TerminationManagementComponent,
    data: { title: 'Create resignation request' }
  },
  {
    path: ':id/update',
    component: TerminationManagementComponent,
    data: { title: 'Update resignation request' }
  },
  {
    path: '',
    component: TerminationListComponent,
    data: { title: 'Resignation requests' }
  }
];

@NgModule({
  imports: [CommonModule, SharedModule, RouterModule.forChild(routes)],
  declarations: [TerminationManagementComponent, TerminationListComponent]
})
export class TerminationManagementModule {}
