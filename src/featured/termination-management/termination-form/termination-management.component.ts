import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';
import { TerminationService } from '../termination.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { RequestForOtherComponent } from 'src/shared/request-for-other/request-for-other.component';

@Component({
  selector: 'app-termination-management',
  templateUrl: './termination-management.component.html',
  styleUrls: ['./termination-management.component.css']
})
export class TerminationManagementComponent implements OnInit {
  @ViewChild('notif') notif: SharedToastComponent;
  @ViewChild('reqforOther') reqforOther: RequestForOtherComponent;

  public formGroup: FormGroup;
  public formSubmitted = false;

  public ID: any = undefined;
  public isUpdate = false;

  public data: any[];
  securityObject: any;

  constructor(
    private service: TerminationService,
    private fb?: FormBuilder,
    private router?: Router,
    private location?: Location,
    private actRoute?: ActivatedRoute
  ) {
    this.formGroup = this.fb.group({
      id: [''],
      reason_for_terminate: ['', Validators.required]
    });

    if (localStorage.getItem('selfServiceAccessToken')) {
      this.securityObject = JSON.parse(
        localStorage.getItem('selfServiceAccessToken')
      );
    }
  }

  ngOnInit() {
    this.ID = this.actRoute.snapshot.params.id;
    if (this.ID !== undefined) {
      this.isUpdate = true;
      this.service.get(this.ID).subscribe((data: any) => {
        data.step > 1 ? this.formGroup.disable() : null;
        delete data.step;
        this.initializeForm(data);
      });
    }
  }

  initializeForm(data) {
    this.formGroup = this.fb.group({
      id: [data.id],
      reason_for_terminate: [data.reason_for_terminate, Validators.required]
    });
  }

  Submit(): any {
    if (!this.formGroup.valid) {
      this.formSubmitted = true;
      return;
    } else {
      if (this.isUpdate) {
        return this.service
          .update({
            requested_by: this.securityObject.employee_id,
            emp_id:
              this.reqforOther.canRequestForOther && this.reqforOther.checked
                ? this.reqforOther.employeeReqId
                : this.securityObject.employee_id,
            data: JSON.stringify(this.formGroup.value)
          })
          .subscribe((response: any) => {
            if (response.status === true) {
              this.router.navigate(['/workspace/termination'], {
                state: {
                  data: {
                    title: 'Success',
                    content: response.message,
                    cssClass: 'e-toast-success'
                  }
                }
              });
            } else {
              this.notif.Show({
                title: 'Error',
                content: response.message,
                cssClass: 'e-toast-danger'
              });
            }
          });
      } else {
        return this.service
          .create({
            requested_by: this.securityObject.employee_id,
            emp_id:
              this.reqforOther.canRequestForOther && this.reqforOther.checked
                ? this.reqforOther.employeeReqId
                : this.securityObject.employee_id,
            action_id: '6',
            data: JSON.stringify(this.formGroup.value)
          })
          .subscribe((response: any) => {
            if (response.status === true) {
              this.router.navigate(['/workspace/termination'], {
                state: {
                  data: {
                    title: 'Success',
                    content: response.message,
                    cssClass: 'e-toast-success'
                  }
                }
              });
            } else {
              this.notif.Show({
                title: 'Error',
                content: response.message,
                cssClass: 'e-toast-danger'
              });
            }
          });
      }
    }
  }

  cancel() {
    this.location.back();
  }
  
}
