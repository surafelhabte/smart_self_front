import { Component, OnInit } from '@angular/core';
import { VacancyService } from '../vacancy.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  public vacancies: any;

  constructor(private service: VacancyService, private route: Router) {}

  ngOnInit() {
    this.service.gets().subscribe((data: any) => {
      this.vacancies = data;
    });
  }

}
