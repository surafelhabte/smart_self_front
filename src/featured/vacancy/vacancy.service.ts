import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VacancyService {
  
  private url = environment.baseUrl;
  
  constructor(private http?: HttpClient) { }
  
  apply(data: any): any {
    return this.http.post(this.url + 'vacancy/vacancy/apply', data);
  }
  
  withdraw(data: any) {
    return this.http.post(this.url + 'vacancy/vacancy/withdraw', data);
  }

  get(data: any) {
    return this.http.post(this.url + 'vacancy/vacancy/Get', data);
  }

  gets() {
    return this.http.get(this.url + 'vacancy/vacancy/Gets');
  }
  
}
