import { Component, OnInit, ViewChild } from '@angular/core';
import { VacancyService } from '../vacancy.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  @ViewChild('notif') notif: SharedToastComponent;
  public securityObject: any;

  public id: any = undefined;
  public detail: any;
  public result: any[];

  constructor(private service: VacancyService,private router?: Router,private location?: Location,private act?: ActivatedRoute) {
    if (localStorage.getItem('selfServiceAccessToken')) {
      this.securityObject = JSON.parse(localStorage.getItem('selfServiceAccessToken'));
    }
  }

  ngOnInit() {
    this.id = this.act.snapshot.params.id;
    if (this.id !== undefined) {
      this.service.get({ id: this.id, employee_id: this.securityObject.employee_id }).subscribe((data: any) => {
        this.detail = data;
        this.detail.Field_Of_Study = JSON.parse(this.detail.Field_Of_Study); 
        this.detail.Sector = JSON.parse(this.detail.Sector); 
        this.detail.Work_Experience = JSON.parse(this.detail.Work_Experience); 
      });

    }
  }

  Submit(): any {
    if(confirm('Are You Sure You Want to Send Application ?')){
      this.result = [];
      this.service.apply({ Employee_id: this.securityObject.employee_id,VacancyId: this.id })
      .subscribe((response: any) => {
          if (response.status) {
            this.router.navigate(['/workspace/vacancy'], {
              state: {data: {title: 'Success',content: response.message,cssClass: 'e-toast-success'}}
            });
  
          } else {
            this.result = response.message;
          }
      });
    }

  }

  Withdraw(): any {
    if(confirm('Are You Sure You Want to Withdraw from this Recruitment ?')){
      this.service.withdraw({ Employee_id: this.securityObject.employee_id, VacancyId: this.id })
      .subscribe((response: any) => {
          if (response.status) {
            this.router.navigate(['/workspace/vacancy'], {
              state: {data: {title: 'Success',content: response.message,cssClass: 'e-toast-success'}}
            });
  
          } else {
            this.notif.Show({ title: 'Error', content: response.message, cssClass: 'e-toast-danger' });
          }
      });
    }

  }

  cancel() {
    this.location.back();
  }

}
