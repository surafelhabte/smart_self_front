import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { HtmlSanitaizerModule } from '../news/htmlSanitaizer/html-sanitaizer.module';

const routes: Routes = [
  {
    path: '',
    component: ListComponent,
    data: { title: 'Internal Vacancy' }
  },
  {
    path: ':id/detail',
    component: DetailComponent,
    data: { title: 'Internal Vacancy Detail' }
  }
];

@NgModule({
  declarations: [ListComponent, DetailComponent],
  imports: [
    CommonModule, SharedModule,HtmlSanitaizerModule, RouterModule.forChild(routes)
  ]
})
export class VacancyModule { }
