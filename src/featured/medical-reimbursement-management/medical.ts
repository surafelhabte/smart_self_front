export class MedicalFor {
  requestFor: string;
}

export class Particulars {
  id?: number;
  date: Date;
  no_of_receipts: number;
  particulars: string;
  amount: string;
  refund_for_id: string;
  institution_name: string;
  institution_address: string;
  attachement: string;
  total_paid: number;
}

export class MedicalRequest {
  request_id?: string;
  action_id: number;
  emp_id: string;
  requested_by: string;
  data: Particulars[] = [];
}

export class TotalRequests {
  totalRequest: MedicalRequest[] = [];
}
