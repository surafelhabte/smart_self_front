import { MedicalService } from './medical.service';
import { TestBed } from '@angular/core/testing';
import {
  HttpTestingController,
  HttpClientTestingModule
} from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from 'src/shared/shared.module';

describe('Service: Medical reimbursement', () => {
  let mService: MedicalService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule, SharedModule]
    });

    mService = TestBed.get(MedicalService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('Should get and list filtered requests ', () => {
    const workflow = [
      {
        id: '29ea152d-53c5-11ea-857b-8cec4ba694b1',
        name: 'Medical Refund',
        status: null,
        step: '1',
        created_date: '2020-02-20 12:41:32'
      },
      {
        id: '29ea152d-53c5-11ea-857b-8cec4ba694b1',
        name: 'Medical Refund',
        status: null,
        step: '1',
        created_date: '2020-02-20 12:41:32'
      }
    ];

    mService
      .getMyRequests({ action_id: 2, emp_id: 'EP123' })
      .subscribe((data: any) => {
        expect(data).toBe(workflow);
      });
    // telling the httmock what kind of request we expect and toward which url
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');

    // fire the request with its data we really expect

    req.flush(workflow);

    httpMock.verify();
  });
  it('Should create medical request', () => {
    const medcial = {
      action_id: '1',
      workflow: [
        { step: 1, authorized_by: 'Manager' },
        { step: 2, authorized_by: 'Officers' }
      ]
    };
    mService.create(medcial).subscribe((data: any) => {
      expect(data.workflow.length).toBeGreaterThan(0);
    });
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');
    req.flush(medcial);
    httpMock.verify();
  });

  it('Should get medcial request successfull', () => {
    const medical = {
      action_id: '2',
      workflow: [
        { step: 1, authorized_by: 'Manager' },
        { step: 2, authorized_by: 'Officers' }
      ]
    };

    mService.getMedicalRequest(medical.action_id).subscribe((data: any) => {
      expect(data).toBe(medical);
    });
    // telling the httmock what kind of request we expect and toward which url
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('GET');

    // fire the request with its data we really expect

    req.flush(medical);

    httpMock.verify();
  });

  it('Should update medical request', () => {
    const medical = {
      action_id: '1',
      workflow: [
        { step: 1, authorized_by: 'Manager' },
        { step: 2, authorized_by: 'Officers' }
      ]
    };

    mService.update(medical).subscribe((data: any) => {
      expect(data.activity.action).toBe('1');
    });
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');

    req.flush(medical);

    httpMock.verify();
  });

  it('Should delete medical request', () => {
    mService.deleteMedicalRequest(12).subscribe((data: any) => {
      expect(data).toBe(12);
    });

    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('DELETE');

    req.flush(12);

    httpMock.verify();
  });
});
