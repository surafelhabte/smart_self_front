import {
  ComponentFixture,
  TestModuleMetadata,
  TestBed,
  async
} from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from 'src/shared/shared.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MedicalReiburementFormComponent } from './medical-reiburement-form.component';
import { MedicalService } from '../medical.service';

describe('MedicalReiburementFormComponent', () => {
  let component: MedicalReiburementFormComponent;
  let fixture: ComponentFixture<MedicalReiburementFormComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [
        SharedModule,
        ReactiveFormsModule,
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [MedicalReiburementFormComponent],
      providers: [MedicalService]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(MedicalReiburementFormComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    component.securityObject = {
      username: 'naol'
    };
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('on submit', () => {
    it('Submit should be called', () => {
      const submitFun = spyOn(component, 'onSubmit');
      component.onSubmit();
      expect(submitFun).toHaveBeenCalled();
    });
  });

  describe('Medical reimbursement form', () => {
    it('Should be created', () => {
      const obj1 = {
        createForm: () => null
      };
      spyOn(obj1, 'createForm');
      obj1.createForm();
      expect(obj1.createForm).toBeTruthy();
    });

    it('Should be invalid when empty', () => {
      expect(component.medicalForm.valid).toBeFalsy();
    });
    
    it('medical reimburesemnt form fields validity sample for family field', () => {
      const reqField = component.requestFor;
      expect(reqField.valid).toBeFalsy();
      reqField.setValue('family');
      expect(reqField.valid).toBeTruthy();
    });

  });
});
