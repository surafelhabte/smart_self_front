import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormArray,
  FormControl
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MedicalService } from '../medical.service';
import { MedicalRequest } from '../medical';
import { Location } from '@angular/common';
import { RequestForOtherComponent } from 'src/shared/request-for-other/request-for-other.component';
import { UploaderComponent } from '@syncfusion/ej2-angular-inputs';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-medical-reiburement-form',
  templateUrl: './medical-reiburement-form.component.html',
  styleUrls: ['./medical-reiburement-form.component.css']
})
export class MedicalReiburementFormComponent implements OnInit {
  @ViewChild('defaultupload') defaultupload: UploaderComponent;


  public familyMembers;
  @ViewChild('notif') notif: SharedToastComponent;
  @ViewChild('reqforOther') reqforOther: RequestForOtherComponent;
  public sysMessage: any = { show: false, content: '', cssStyle: '' };
  medicalForm: FormGroup;
  particularForm: FormGroup;
  public isUpdate = false;
  formarrayValidity = true;
  actionId = 3;
  medicalId: string;
  amountInfo = {
    totalPayment: null,
    totalExpenditure: null
  };
  public medicalBalance: any;
  public fields: any = { text: 'full_name', value: 'id' };
  public particularFields: any = { text: 'name', value: 'name' };
  submitted: boolean;
  public totalExpenditure = 0;
  public totalPayment = 0;
  public particulars = [
    { id: 1, name: 'Medical Examination' },
    { id: 2, name: 'Laboratory' },
    { id: 3, name: 'Medicine' },
    { id: 4, name: 'Hospital(Bed ... etc.)' },
    { id: 5, name: 'Other Related Expenses' }
  ];
  securityObject: any;
  public today: Date = new Date();
  public maxDate: Object =  new Date(this.today.getFullYear(),this.today.getMonth(),this.today.getDate());
  public minDate: Object =  new Date(this.today.getFullYear() - 1,null,null);

  public attachements: any[] = [];


  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private apiService: MedicalService,
    private location: Location,
    private router: Router
  ) {
    this.createForm();
    if (localStorage.getItem('selfServiceAccessToken')) {
      this.securityObject = JSON.parse(
        localStorage.getItem('selfServiceAccessToken')
      );
    }
  }

  ngOnInit() {
    this.getFamilyMembers(this.securityObject.employee_id);
    this.medicalId = this.activatedRoute.snapshot.paramMap.get('id');
    if (this.medicalId) {
      this.isUpdate = true;
      this.apiService
        .getMedicalRequest(this.medicalId)
        .subscribe((data: any) => {
          this.initializeForm(data);
          data.step > 1 ? this.medicalForm.disable() : null;
        });
    }
  }

  getFamilyMembers(employeeReqId) {
    this.apiService
      .getFamilies({ emp_id: employeeReqId })
      .subscribe((data: any) => {
        this.familyMembers = data;
      });
  }

  createForm(): void {
    this.medicalForm = this.formBuilder.group({
      requestFor: ['', Validators.required],
      medicalArray: this.formBuilder.array([this.createMedicalArray()])
    });
  }

  initializeForm(med) {
    this.medicalForm = this.formBuilder.group({
      requestFor: [med.refund_for_id ? med.refund_for_id : this.securityObject.employee_id],
      medicalArray: this.formBuilder.array([])
    });
    med.data.forEach((element,index) => {
      this.medicalArray.controls.push(this.initializeMedicalArray(element));
      this.attachements.push(element.attachement)
    });

    
  }

  initializeMedicalArray(emp) {
    return this.formBuilder.group({
      id: [emp.id],
      date: [emp.date, Validators.required],
      particular: [emp.particulars, Validators.required],
      noOfReciepts: [emp.no_of_receipts, Validators.required],
      amount: [emp.amount, Validators.required],
      institution_name: [emp.institution_name, Validators.required],
      institution_address: [emp.institution_address, Validators.required],
      attachement: [environment.baseUrl + emp.attachement]
    });
  }

  createMedicalArray() {
    return this.formBuilder.group({
      date: ['', Validators.required],
      particular: ['', Validators.required],
      noOfReciepts: ['', Validators.required],
      amount: ['', Validators.required],
      institution_name: ['', Validators.required],
      institution_address: ['', Validators.required],
      attachement: ['']
    });
  }

  addArray(): void {
    this.medicalArray.controls.push(this.createMedicalArray());
    this.calculateTotal();
  }

  deleteArray(index) {
    const deletedControlId = this.medicalArray.controls[index].value.id;
    if (deletedControlId) {
      if (confirm('Are you sure you want to delete this item?')) {
        this.apiService.removeFlow(deletedControlId).subscribe((response: any) => {
          if(response.status){
            this.notif.Show({ title: 'Success',content: response.message,cssClass: 'e-toast-success' });
          }
        });
      }
      this.medicalArray.removeAt(index);
      this.attachements.splice(index,1,null);

    } else {
      this.medicalArray.removeAt(index);

    }
  }

  get medicalArray(): FormArray {
    return this.medicalForm.get('medicalArray') as FormArray;
  }

  get requestFor(): FormControl {
    return this.medicalForm.get('requestFor') as FormControl;
  }

  prepareFormData(): MedicalRequest {
    if (this.medicalArray.controls.length > 0) {
      this.medicalArray.controls.forEach(element => {
        this.formarrayValidity = element.valid;
      });
    }
    const medical = new MedicalRequest();
    if (this.medicalForm.valid && this.formarrayValidity) {
      if (this.isUpdate) {
        (medical.request_id = this.medicalId),
          this.medicalArray.controls.forEach((element,index) => {
            medical.data.push({
              date: element.value.date,
              particulars: element.value.particular,
              no_of_receipts: element.value.noOfReciepts,
              amount: element.value.amount,
              institution_name: element.value.institution_name,
              institution_address: element.value.institution_address,
              attachement: element.value.attachement ? element.value.attachement : this.attachements[index],
              refund_for_id: this.requestFor.value.includes('ECX') ? null: this.requestFor.value,
              total_paid: this.amountInfo.totalPayment,
              id: element.value.id
            });
          });
      } else {
        medical.action_id = this.actionId;
        medical.requested_by = this.securityObject.employee_id;
        medical.emp_id = this.reqforOther.canRequestForOther && this.reqforOther.checked ? this.reqforOther.employeeReqId: this.securityObject.employee_id;
            
        this.medicalArray.controls.forEach(element => {
          medical.data.push({
            date: element.value.date,
            particulars: element.value.particular,
            no_of_receipts: element.value.noOfReciepts,
            amount: element.value.amount,
            institution_name: element.value.institution_name,
            institution_address: element.value.institution_address,
            attachement: element.value.attachement,
            refund_for_id: this.requestFor.value.includes('ECX') ? null : this.requestFor.value, 
            total_paid: this.amountInfo.totalPayment
          });
        });
      }
      return medical;

    } else {
      return null;

    }
  }

  onSubmit() {
    const payload = this.prepareFormData();
    this.submitted = true;
    if (payload) {
      if (this.medicalId) {
        this.apiService.update(payload).subscribe((response: any) => {
          if (response.status) {
            this.router.navigate(['/workspace/medical-reimbursement'], {
              state: {
                data: {
                  title: 'Success',
                  content: response.message,
                  cssClass: 'e-toast-success'
                }
              }
            });
          } else {
            this.sysMessage = {
              show: true,
              content: response.message,
              cssStyle: 'danger'
            };
          }
        });
      } else {
        this.apiService.create(payload).subscribe((response: any) => {
          if (response.status) {
            this.router.navigate(['/workspace/medical-reimbursement'], {
              state: {
                data: {
                  title: 'Success',
                  content: response.message,
                  cssClass: 'e-toast-success'
                }
              }
            });
          } else {
            this.notif.Show({
              title: 'Error',
              content: response.message,
              cssClass: 'e-toast-danger'
            });
          }
        });
      }
    }
  }

  cancel() {
    this.location.back();
  }

  showBalance(id) {
    if (id !== undefined) {
      this.familyMembers.forEach(member => {
        if (member.id.indexOf(id) > -1) {
          this.medicalBalance = member.allowed_amount - member.Total;
        }
      });
    }
  }

  calculateTotal() {
    this.totalExpenditure = 0;
    this.medicalArray.controls.forEach(element => {
      this.totalExpenditure = this.totalExpenditure + +element.value.amount;
      this.totalPayment = this.totalExpenditure * 0.95;

      if (this.requestFor.value === this.securityObject.employee_id) {
        this.amountInfo = {
          totalExpenditure: this.totalExpenditure,
          totalPayment:
            this.medicalBalance > this.totalExpenditure * 0.95
              ? this.totalExpenditure * 0.95
              : this.medicalBalance
        };
      } else {
        this.amountInfo = {
          totalExpenditure: this.totalExpenditure,
          totalPayment:
            this.medicalBalance > this.totalExpenditure * 0.9
              ? this.totalExpenditure * 0.9
              : this.medicalBalance
        };
      }
    });
    return this.amountInfo;
  }

  onAttachementSelect(index,file){
    const i = file.filesData;
    if (i.length > 0) {
      const reader = new FileReader();
      reader.onloadend = () => {
        let formGroup: any = this.medicalArray.controls;
        formGroup[index].controls.attachement.setValue(reader.result.toString());
      };
      reader.readAsDataURL(i[0].rawFile);
    }
  }

  onAttachementRemove(index){
    let formGroup: any = this.medicalArray.controls;
    formGroup[index].controls.attachement.setValue(null);
  }

  
}
