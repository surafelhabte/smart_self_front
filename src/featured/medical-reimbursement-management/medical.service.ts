import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MedicalService {
  private url = environment.baseUrl;

  constructor(private http: HttpClient) {}

  getMyRequests(emp_info) {
    return this.http.post(this.url + 'medical/medical/filter', emp_info);
  }

  getFamilies(emp_info) {
    return this.http.post(
      this.url + 'medical/medical/getFamilyMembers/',
      emp_info
    );
  }

  getPositions() {
    return this.http.get(this.url + 'position/position/gets');
  }

  deleteMedicalRequest(deletedId) {
    return this.http.delete(this.url + 'medical/medical/delete/' + deletedId);
  }

  removeFlow(deletedId) {
    return this.http.delete(this.url + 'medical/medical/remove/' + deletedId);
  }

  create(workflow: any): any {
    return this.http.post(this.url + 'medical/medical/create', workflow);
  }

  getMedicalRequest(id: any) {
    return this.http.get(this.url + 'medical/medical/get/' + id);
  }

  update(workflow: any) {
    return this.http.post(this.url + 'medical/medical/Update', workflow);
  }
}
