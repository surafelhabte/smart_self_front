import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MedicalReiburementFormComponent } from './medical-reiburesment-form/medical-reiburement-form.component';
import { MedicalReimbursementListComponent } from './medical-reimbursement-list/medical-reimbursement-list.component';
import { SharedModule } from 'src/shared/shared.module';
import { UploaderModule } from '@syncfusion/ej2-angular-inputs';

const routes: Routes = [
  {
    path: 'create',
    component: MedicalReiburementFormComponent,
    data: { title: 'Create medical refund request' }
  },
  {
    path: ':id/update',
    component: MedicalReiburementFormComponent,
    data: { title: 'Update medical refund request' }
  },
  {
    path: '',
    component: MedicalReimbursementListComponent,
    data: { title: 'Medical refund requests' }
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), SharedModule,UploaderModule],
  declarations: [
    MedicalReiburementFormComponent,
    MedicalReimbursementListComponent
  ]
})
export class MedicalReimbursementManagementModule {}
