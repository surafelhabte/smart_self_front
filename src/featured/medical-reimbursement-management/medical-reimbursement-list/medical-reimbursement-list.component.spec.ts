import { MedicalReimbursementListComponent } from './medical-reimbursement-list.component';
import {
  ComponentFixture,
  TestModuleMetadata,
  TestBed,
  async
} from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from 'src/shared/shared.module';
import { MedicalService } from '../medical.service';

describe('MedicalReimbursementListComponent', () => {
  let component: MedicalReimbursementListComponent;
  let fixture: ComponentFixture<MedicalReimbursementListComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [RouterTestingModule, HttpClientTestingModule, SharedModule],
      providers: [MedicalService],
      declarations: [MedicalReimbursementListComponent]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(MedicalReimbursementListComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    component.securityObject = {
      username: 'naol'
    };
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
