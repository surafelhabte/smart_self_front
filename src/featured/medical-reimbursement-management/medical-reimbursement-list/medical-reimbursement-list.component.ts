import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';
import { Router } from '@angular/router';
import { MedicalService } from '../medical.service';

@Component({
  selector: 'app-medical-reimbursement-list',
  templateUrl: './medical-reimbursement-list.component.html',
  styleUrls: ['./medical-reimbursement-list.component.css']
})
export class MedicalReimbursementListComponent implements OnInit {
  public medicalRequests = [];
  public columnsToDisplay: any[] = [];
  @ViewChild('notif') notif: SharedToastComponent;
  public incomingCommand = { edit: true, delete: true };
  securityObject: any;

  constructor(private apiService: MedicalService, private router: Router) {
    this.columnsToDisplay.push({ field: 'name', headerText: 'Name', textAlign: 'center', width: 90 });
    this.columnsToDisplay.push({ field: 'created_date', headerText: 'Requested date', textAlign: 'center', width: 90 });
    this.columnsToDisplay.push({ field: 'requested_amount', headerText: 'Requested Amount', textAlign: 'center', width: 90 });
    this.columnsToDisplay.push({ field: 'total_paid', headerText: 'Total Paid', textAlign: 'center', width: 90 });
    this.columnsToDisplay.push({ field: 'status', headerText: 'Status', textAlign: 'center', width: 90 });

    if (localStorage.getItem('selfServiceAccessToken')) {
      this.securityObject = JSON.parse(
        localStorage.getItem('selfServiceAccessToken')
      );
    }
  }

  ngOnInit() {
    const empInfo = {
      action_id: 3,
      emp_id: this.securityObject.employee_id
    };
    this.apiService.getMyRequests(empInfo).subscribe((data: any[]) => {
      this.medicalRequests = data;
    });
  }

  add() {
    this.router.navigate(['workspace/medical-reimbursement/create']);
  }

  edit($event) {
    this.router.navigate(['workspace/medical-reimbursement/' + $event.id + '/update']);
  }

  delete($event) {
    const conf = confirm('Are you sure you want to delete this item?');
    if (conf) {
      this.apiService
        .deleteMedicalRequest($event.id)
        .subscribe((response: any) => {
          if (response.status === true) {
            this.notif.Show({
              title: 'Success',
              content: response.message,
              cssClass: 'e-toast-success'
            });
            this.ngOnInit();
          } else {
            this.notif.Show({
              title: 'Error',
              content: response.message,
              cssClass: 'e-toast-danger'
            });
          }
        });
    }
  }
}
