import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsefulLinkService {

  private url = environment.baseUrl + 'dashboard/dashboard';

  constructor(private http?: HttpClient) { }

  get() {
    return this.http.get(this.url + '/GetLinks');
  }

}
