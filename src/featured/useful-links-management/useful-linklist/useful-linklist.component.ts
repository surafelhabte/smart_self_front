import { Component, OnInit } from '@angular/core';
import { UsefulLinkService } from '../useful-link.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-useful-linklist',
  templateUrl: './useful-linklist.component.html',
  styleUrls: ['./useful-linklist.component.css']
})
export class UsefulLinklistComponent implements OnInit {

  public links: any;

  constructor(private service: UsefulLinkService, private route: Router) {}

  ngOnInit() {
    this.service.get().subscribe((data: any) => {
      this.links = data;
    });
  }

}
