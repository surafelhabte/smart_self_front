/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import {
  HttpTestingController,
  HttpClientTestingModule
} from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { DashboardService } from './dashboard.service';

describe('Service: Dashboard', () => {
  let nService: DashboardService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule]
    });

    nService = TestBed.get(DashboardService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('Should get and list notification ', () => {
    const notifs = [
      {
        medical_balance: 100,
        leave_balance: 20
      }
    ];

    nService.getBalance('EP123').subscribe((data: any) => {
      expect(data).toBe(notifs);
    });
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');
    req.flush(notifs);
    httpMock.verify();
  });
});
