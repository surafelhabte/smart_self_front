import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardManagementComponent } from './dashboard-management.component';
import { Routes, RouterModule } from '@angular/router';
import { NewsListComponent } from '../news/news-list/news-list.component';
import { UsefulLinklistComponent } from '../useful-links-management/useful-linklist/useful-linklist.component';
import { NgbModule, NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

const routes: Routes = [
  {
    path: '',
    component: DashboardManagementComponent,
    data: { title: 'Dashboard'}
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), NgbModule],
  declarations: [
    DashboardManagementComponent,
    NewsListComponent,
    UsefulLinklistComponent
  ],
  providers: [NgbCarouselConfig]
})
export class DashboardManagementModule {}
