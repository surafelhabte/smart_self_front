import { Component, OnInit, ViewChild } from '@angular/core';
import { DashboardService } from './dashboard.service';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-dashboard-management',
  templateUrl: './dashboard-management.component.html',
  styleUrls: ['./dashboard-management.component.css']
})
export class DashboardManagementComponent implements OnInit {
  constructor(
    private apiService: DashboardService,
    private config: NgbCarouselConfig
  ) {
    if (localStorage.getItem('selfServiceAccessToken')) {
      this.securityObject = JSON.parse(
        localStorage.getItem('selfServiceAccessToken')
      );
    }
  }
  weeklyAttendance: any;
  public medicalBalance: any;
  public leaveBalance = [];
  public keys: any;
  public values: any;
  securityObject: any;
  sliderImages = [];

  ngOnInit() {
    this.config.interval = 2000;
    this.config.wrap = true;
    this.config.keyboard = false;
    this.config.pauseOnHover = false;
    this.apiService
      .getBalance({ emp_id: this.securityObject.employee_id })
      .subscribe((balance: any) => {
        balance.forEach(element => {
          if (element.balance) {
            this.leaveBalance.push(element);
          }
        });
      });

    this.apiService
      .getGallery()
      .subscribe((data: any) => {
        this.sliderImages = data;
      });

    this.apiService
      .getMedicalBalance({ emp_id: this.securityObject.employee_id })
      .subscribe((balance: any) => {
        this.medicalBalance = balance.amount - balance.total;
      });

    this.apiService
      .getWeeklyAttendance(this.securityObject.employee_id)
      .subscribe((data: any) => {
        this.weeklyAttendance = data;
      });
  }
}
