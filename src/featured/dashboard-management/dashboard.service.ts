import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  private url = environment.baseUrl;

  constructor(private http?: HttpClient) {}

  getBalance(empId) {
    return this.http.post(this.url + 'leave/leave/getLeaveType', empId);
  }

  getGallery() {
    const url = this.url + 'dashboard/dashboard';
    return this.http.get(url + '/GetPhotos');
  }

  getMedicalBalance(empId) {
    return this.http.post(this.url + 'medical/medical/getBalance', empId);
  }

  getWeeklyAttendance(empId) {
    return this.http.post(this.url + 'attendance/attendance/GetWeekly/', {
      emp_id: empId
    });
  }
}
