import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AttendanceService {
  private url = environment.baseUrl;

  constructor(private http: HttpClient) {}

  getAttendanceReport(filterInfo: any) {
    return this.http.post(this.url + 'attendance/attendance/gets', filterInfo);
  }

  getWeekly(filterInfo: any) {
    return this.http.post(this.url + 'attendance/attendance/GetWeekly', filterInfo);
  }

}
