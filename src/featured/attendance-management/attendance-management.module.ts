import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AttendanceReportComponent } from './attendance-report/attendance-report.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: AttendanceReportComponent,
    data: { title: 'Attendance' }
  }
];
@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), SharedModule],
  declarations: [AttendanceReportComponent]
})
export class AttendanceManagementModule {}
