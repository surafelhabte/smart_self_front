import { Component, OnInit, ViewChild } from '@angular/core';
import { AttendanceService } from '../attendance.service';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';

@Component({
  selector: 'app-attendance-report',
  templateUrl: './attendance-report.component.html',
  styleUrls: ['./attendance-report.component.css']
})
export class AttendanceReportComponent implements OnInit {
  atForm: FormGroup;
  range: any;
  @ViewChild('notif') notif: SharedToastComponent;
  attendanceReport = [];
  public columnsToDisplay: any[] = [];
  securityObject: any;
  submitted: boolean;

  constructor(
    private apiService: AttendanceService,
    private formBuilder: FormBuilder
  ) {
    this.columnsToDisplay.push({
      field: 'scan_date',
      headerText: 'Date',
      textAlign: 'center',
      width: 60
    });
    this.columnsToDisplay.push({
      field: 'start_time',
      headerText: 'Morning In',
      textAlign: 'center',
      width: 70
    });
    this.columnsToDisplay.push({
      field: 'start_break',
      headerText: 'Morning out',
      textAlign: 'center',
      width: 70
    });
    this.columnsToDisplay.push({
      field: 'end_break',
      headerText: 'Afternoon In',
      textAlign: 'center',
      width: 70
    });
    this.columnsToDisplay.push({
      field: 'end_time',
      headerText: 'Afternoon Out',
      textAlign: 'center',
      width: 70
    });
    this.columnsToDisplay.push({
      field: 'attendance_status',
      headerText: 'Status',
      textAlign: 'center',
      width: 40
    });
    this.columnsToDisplay.push({
      field: 'attendance_description',
      headerText: 'Description',
      textAlign: 'center',
      width: 60
    });
    this.columnsToDisplay.push({
      field: 'total_work_time',
      headerText: 'Total work time',
      textAlign: 'center',
      width: 70
    });
    this.columnsToDisplay.push({
      field: 'working_time',
      headerText: 'Working time',
      textAlign: 'left',
      width: 60
    });
    this.columnsToDisplay.push({
      field: 'absent_hour',
      headerText: 'Absent hour',
      textAlign: 'left',
      width: 60
    });

    if (localStorage.getItem('selfServiceAccessToken')) {
      this.securityObject = JSON.parse(
        localStorage.getItem('selfServiceAccessToken')
      );
    }
    this.createForm();
  }

  ngOnInit() {
    this.range = JSON.parse(localStorage.getItem('range'));
    if (this.range) {
      this.initializeForm(this.range);
      this.apiService
        .getAttendanceReport(this.range)
        .subscribe((response: any) => {
          this.attendanceReport = response;
        });
    } else {
    }
  }

  createForm() {
    this.atForm = this.formBuilder.group({
      from: [''],
      to: ['']
    });
  }

  initializeForm(range) {
    this.atForm = this.formBuilder.group({
      from: [range.start_date],
      to: [range.end_date]
    });
  }

  get from(): FormControl {
    return this.atForm.get('from') as FormControl;
  }

  get to(): FormControl {
    return this.atForm.get('to') as FormControl;
  }

  prepareFormData() {
    let range: any;
    if (this.atForm.valid) {
      return {
        emp_id: this.securityObject.employee_id,
        start_date: this.from.value,
        end_date: this.to.value
      };
    } else {
      return null;
    }
  }

  onSubmit() {
    const payload = this.prepareFormData();
    this.submitted = true;
    localStorage.setItem('range', JSON.stringify(payload));
    if (payload) {
      this.apiService
        .getAttendanceReport(payload)
        .subscribe((response: any) => {
          this.ngOnInit();
        });
    }
  }
}
