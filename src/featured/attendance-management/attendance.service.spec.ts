import { TestBed } from '@angular/core/testing';
import {
  HttpTestingController,
  HttpClientTestingModule
} from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from 'src/shared/shared.module';
import { AttendanceService } from './attendance.service';

describe('Service: Attendance', () => {
  let mService: AttendanceService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule, SharedModule]
    });

    mService = TestBed.get(AttendanceService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('Should get and list attendance report ', () => {
    const workflow = [
      {
        id: '29ea152d-53c5-11ea-857b-8cec4ba694b1',
        name: 'Medical Refund',
        status: null,
        step: '1',
        created_date: '2020-02-20 12:41:32'
      },
      {
        id: '29ea152d-53c5-11ea-857b-8cec4ba694b1',
        name: 'Medical Refund',
        status: null,
        step: '1',
        created_date: '2020-02-20 12:41:32'
      }
    ];

    mService
      .getAttendanceReport({ action_id: 2, emp_id: 'EP123' })
      .subscribe((data: any) => {
        expect(data).toBe(workflow);
      });
    // telling the httmock what kind of request we expect and toward which url
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');

    // fire the request with its data we really expect

    req.flush(workflow);

    httpMock.verify();
  });
});
