import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeaveFormComponent } from './leave-form/leave-form.component';
import { LeaveListComponent } from './leave-list/leave-list.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/shared/shared.module';
import { LeaveScheduleFormComponent } from './leave-schedule-form/leave-schedule-form.component';
import { LeaveScheduleListComponent } from './leave-schedule-list/leave-schedule-list.component';

const routes: Routes = [
  {
    path: '',
    component: LeaveListComponent,
    data: { title: 'Leave Requests' }
  },
  {
    path: 'create',
    component: LeaveFormComponent,
    data: { title: 'Create Leave Request' }
  },
  {
    path: 'schedule',
    component: LeaveScheduleListComponent,
    data: { title: 'Leave Schedule' }
  },
  {
    path: 'schedule/create',
    component: LeaveScheduleFormComponent,
    data: { title: 'Create Leave Schedule' }
  },
  {
    path: 'schedule/:id/update',
    component: LeaveScheduleFormComponent,
    data: { title: 'Update  Leave Request' }
  },
  {
    path: ':id/update',
    component: LeaveFormComponent,
    data: { title: 'Update  Leave Request' }
  }
];

@NgModule({
  imports: [CommonModule, SharedModule, RouterModule.forChild(routes)],
  declarations: [LeaveFormComponent, LeaveListComponent, LeaveScheduleFormComponent, LeaveScheduleListComponent]
})
export class LeaveManagementModule {}
