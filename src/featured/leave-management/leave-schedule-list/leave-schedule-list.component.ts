import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { LeaveService } from '../leave.service';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';

@Component({
  selector: 'app-leave-schedule-list',
  templateUrl: './leave-schedule-list.component.html',
  styleUrls: ['./leave-schedule-list.component.css']
})
export class LeaveScheduleListComponent implements OnInit {
  @ViewChild('notif') notif: SharedToastComponent;

  public columns: any[] = [];
  public Command = { edit: true, delete: true };
  
  public schedules = [];
  public securityObject: any;

  constructor(private Service: LeaveService, private router: Router) {
    this.columns.push({ field: 'budget_year_start', headerText: 'Start Date', textAlign: 'center', width: 90 });
    this.columns.push({ field: 'budget_year_end', headerText: 'End Date', textAlign: 'center', width: 90 });
    this.columns.push({ field: 'months', headerText: 'Month', textAlign: 'center', width: 90 });

    if (localStorage.getItem('selfServiceAccessToken')) {
      this.securityObject = JSON.parse(localStorage.getItem('selfServiceAccessToken'));
    }

  }

  ngOnInit() {
    this.Service.Schedules({ employee_id : this.securityObject.employee_id }).subscribe((data: any[]) => {
      this.schedules = data;
    });
  }

  add() {
    this.router.navigate(['workspace/leave/schedule/create']);
  }

  edit($event) {
    this.router.navigate(['workspace/leave/schedule/' + $event.id + '/update']);
  }

  delete($event) {
    const conf = confirm('Are you sure you want to delete this item?');
    if (conf) {
      this.Service
        .deleteSchedule($event.id)
        .subscribe((response: any) => {
          if (response.status === true) {
            this.notif.Show({
              title: 'Success',
              content: response.message,
              cssClass: 'e-toast-success'
            });
            this.ngOnInit();
          } else {
            this.notif.Show({
              title: 'Error',
              content: response.message,
              cssClass: 'e-toast-danger'
            });
          }
        });
    }
  }

}
