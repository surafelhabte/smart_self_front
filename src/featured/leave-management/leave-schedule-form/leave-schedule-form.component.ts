import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';
import { RequestForOtherComponent } from 'src/shared/request-for-other/request-for-other.component';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { LeaveService } from '../leave.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';


@Component({
  selector: 'app-leave-schedule-form',
  templateUrl: './leave-schedule-form.component.html',
  styleUrls: ['./leave-schedule-form.component.css']
})
export class LeaveScheduleFormComponent implements OnInit {

  @ViewChild('notif') notif: SharedToastComponent;
  @ViewChild('reqforOther') reqforOther: RequestForOtherComponent;

  public formGroup: FormGroup;
  public formSubmitted = false;

  public id: any = undefined;
  public remainingBalance : number = 0;
  public isUpdate = false;

  public today: Date = new Date();
  public minDate: Object =  new Date(this.today.getFullYear(),null,null);
  public maxDate: Object =  new Date(this.today.getFullYear(),12,null);
  public securityObject: any;
  public employee_id: any;

  constructor(private service: LeaveService,private fb?: FormBuilder,private router?: Router,private location?: 
    Location,private actRoute?: ActivatedRoute) {
  
    this.formGroup = this.fb.group({
      schedules : this.fb.array([])
    });

    if (localStorage.getItem('selfServiceAccessToken')) {
      this.securityObject = JSON.parse(localStorage.getItem('selfServiceAccessToken'));
      this.employee_id = this.securityObject.employee_id;
    }

  }

  ngOnInit() {
    this.load_leave_type({ employee_id: this.securityObject.employee_id });
    this.id = this.actRoute.snapshot.params.id;
    if (this.id !== undefined) {
      this.isUpdate = true;
      this.service.Schedule(this.id).subscribe((data: any[]) => {
        data.forEach(element => {
          this.add(element);
        });
      });

    } else {
      this.add();
    }
  }

  get schedules() {
    return this.formGroup.get('schedules') as FormArray;
  }

  add(data?: any) {
    this.schedules.push(
      this.fb.group({
        id: [data ? data.id : null],
        employee_id: [this.employee_id],
        budget_year_start: [data ? data.budget_year_start : '', Validators.required],
        budget_year_end: [data ? data.budget_year_end : '', Validators.required],
      })
    );
    
    let schedule = this.formGroup.get('schedules') as FormArray;
    if(schedule.length > 1){
      this.minDate = new Date(schedule.at(schedule.length - 2).value.budget_year_end);
    }
  }

  remove(index: number) {
    if (confirm('Are you sure you want to delete')) {
      this.schedules.removeAt(index);

      let schedule = this.formGroup.get('schedules') as FormArray;
      if(schedule.length === 1){
        this.minDate = new Date(this.today.getFullYear(),null,null);
      }
    }
  }

  get_forGroup(index): FormGroup {
    return this.schedules.controls[index] as FormGroup;
  }

  Submit(): any {
    this.formSubmitted = true;

    if(this.formGroup.invalid) {
      return;

    } else {
      if(this.total_Days() > this.remainingBalance){
        this.notif.Show({
          title: 'Invalid Amount',content: 'You enter higher total schedule date than your remaining balance. please fix this before you send.',cssClass: 'e-toast-info'
        });
        return

      } else {
        if (this.isUpdate) {
          return this.service
            .updateSchedule({ data: JSON.stringify(this.formGroup.value) })
            .subscribe((response: any) => {
              if (response.status === true) {
                this.router.navigate(['/workspace/leave/schedule'], {
                  state: {
                    data: {
                      title: 'Success',
                      content: response.message,
                      cssClass: 'e-toast-success'
                    }
                  }
                });
              } else {
                this.notif.Show({
                  title: 'Error',
                  content: response.message,
                  cssClass: 'e-toast-danger'
                });
              }
            });
        } else {
          return this.service
            .createSchedule({ data: JSON.stringify(this.formGroup.value) })
            .subscribe((response: any) => {
              if (response.status === true) {
                this.router.navigate(['/workspace/leave/schedule'], {
                  state: {
                    data: {
                      title: 'Success',
                      content: response.message,
                      cssClass: 'e-toast-success'
                    }
                  }
                });
              } else {
                this.notif.Show({
                  title: 'Error',
                  content: response.message,
                  cssClass: 'e-toast-danger'
                });
              }
            });
        } 
      }

    }
  }

  cancel() {
    this.location.back();
  }

  load_leave_type(employee_id) {
    this.service.actual_balance(employee_id).subscribe((data: any) => {
      this.remainingBalance = data;
    });
  }

  total_Days(): number{
    let total = 0;
    this.schedules.controls.forEach(element => {
      if(element.valid){
        let FG: any = element;
        const date1 = new Date(FG.controls.budget_year_start.value); 
        const date2 = new Date(FG.controls.budget_year_end.value); 
        
        let Difference_In_Time = date2.getTime() - date1.getTime(); 
        let Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24); 
        
        total += Difference_In_Days;

      }
    });
    return total;
  }

}