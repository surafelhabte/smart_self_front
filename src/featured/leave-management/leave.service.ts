import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LeaveService {

  private url = environment.baseUrl + 'leave/leave/';

  constructor(private http?: HttpClient) { }

  create(leave: any): any {
    return this.http.post(this.url + 'create', leave);
  }

  filter(filter) {
    return this.http.post(this.url + 'filter', filter);
  }

  get(id: any) {
    return this.http.get(this.url + 'get/' + id);
  }

  
  update(leave: any) {
    return this.http.post(this.url + 'update', leave);
  }
  
  delete(id: any) {
    return this.http.delete(this.url + 'delete/' + id);
  }

  getBalance(data: any) {
    return this.http.post(this.url + 'GetBalance', data);
  }

  loadLeaveType(emp_id: any) {
    return this.http.post(this.url + 'GetLeaveType', emp_id);
  }

  //schedules
  createSchedule(schedule: any): any {
    return this.http.post(this.url + 'schedule', schedule);
  }

  updateSchedule(schedule: any) {
    return this.http.put(this.url + 'schedule', schedule);
  }
  
  Schedules(employee_id: any) {
    return this.http.post(this.url + 'schedules/', employee_id);
  }

  Schedule(id: any) {
    return this.http.get(this.url + 'schedule/' + id);
  }

  deleteSchedule(id: any) {
    return this.http.delete(this.url + 'schedule/' + id);
  }

  actual_balance(employee_id){
    return this.http.post(this.url + 'actual_balance', employee_id);
  }

}
