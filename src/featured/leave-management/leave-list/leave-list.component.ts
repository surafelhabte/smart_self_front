import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';
import { LeaveService } from '../leave.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-leave-list',
  templateUrl: './leave-list.component.html',
  styleUrls: ['./leave-list.component.css']
})
export class LeaveListComponent implements OnInit {
  public leaves = [];
  public columnsToDisplay: any[] = [];

  @ViewChild('notif') notif: SharedToastComponent;
  public incomingCommand = { edit: true, delete: true };
  securityObject: any;

  constructor(private apiService: LeaveService, private router: Router) {
    this.prepare();

    if (localStorage.getItem('selfServiceAccessToken')) {
      this.securityObject = JSON.parse(
        localStorage.getItem('selfServiceAccessToken')
      );
    }
  }

  ngOnInit() {
    this.apiService
      .filter({ action_id: '5', emp_id: this.securityObject.employee_id })
      .subscribe((data: any[]) => {
        this.leaves = data;
      });
  }

  add() {
    this.router.navigate(['workspace/leave/create']);
  }

  edit($event) {
    this.router.navigate(['workspace/leave/' + $event.id + '/update']);
  }

  delete($event) {
    const conf = confirm('Are you sure you want to delete this request ?');
    if (conf) {
      this.apiService.delete($event.id).subscribe((response: any) => {
        if (response.status === true) {
          this.notif.Show({
            title: 'Success',
            content: response.message,
            cssClass: 'e-toast-success'
          });
          this.ngOnInit();
        } else {
          this.notif.Show({
            title: 'Error',
            content: response.message,
            cssClass: 'e-toast-danger'
          });
        }
      });
    }
  }

  prepare() {
    this.columnsToDisplay.push({
      field: 'type',
      headerText: 'Leave Type',
      textAlign: 'center',
      width: 50
    });
    this.columnsToDisplay.push({
      field: 'total',
      headerText: 'Total Day',
      textAlign: 'center',
      width: 50
    });
    this.columnsToDisplay.push({
      field: 'status',
      headerText: 'Status',
      textAlign: 'center',
      width: 50
    });
    this.columnsToDisplay.push({
      field: 'created_date',
      headerText: 'Request date',
      textAlign: 'center',
      width: 50
    });
  }
}
