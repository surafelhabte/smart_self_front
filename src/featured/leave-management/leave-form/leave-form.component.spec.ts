import {
  ComponentFixture,
  TestModuleMetadata,
  TestBed,
  async
} from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from 'src/shared/shared.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LeaveFormComponent } from './leave-form.component';
import { LeaveService } from '../leave.service';

describe('LeaveFormComponent', () => {
  let component: LeaveFormComponent;
  let fixture: ComponentFixture<LeaveFormComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [
        SharedModule,
        ReactiveFormsModule,
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [LeaveFormComponent],
      providers: [LeaveService]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(LeaveFormComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    component.securityObject = {
      username: 'naol'
    };
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('on submit', () => {
    it('Submit should be called', () => {
      const submitFun = spyOn(component, 'Submit');
      component.Submit();
      expect(submitFun).toHaveBeenCalled();
    });
  });

  describe('Leave request form', () => {
    it('Should be created', () => {
      const obj1 = {
        createForm: () => null
      };
      spyOn(obj1, 'createForm');
      obj1.createForm();
      expect(obj1.createForm).toBeTruthy();
    });

    it('Should be invalid when empty', () => {
      expect(component.formGroup.valid).toBeFalsy();
    });
    it('leave request form fields validity sample for family field', () => {
      const reqField = component.formGroup.controls.from;
      expect(reqField.valid).toBeFalsy();
      reqField.setValue('family');
      expect(reqField.valid).toBeTruthy();
    });
  });
});
