import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LeaveService } from '../leave.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';
import { Location } from '@angular/common';
import { RequestForOtherComponent } from 'src/shared/request-for-other/request-for-other.component';

@Component({
  selector: 'app-leave-form',
  templateUrl: './leave-form.component.html',
  styleUrls: ['./leave-form.component.css']
})
export class LeaveFormComponent implements OnInit {
  @ViewChild('notif') notif: SharedToastComponent;
  @ViewChild('reqforOther') reqforOther: RequestForOtherComponent;

  public formGroup: FormGroup;
  public formSubmitted = false;
  public fields: any = { text: 'leave_name', value: 'id' };
  public employeeId: string;

  public ID: any = undefined;
  public remainingBalance = undefined;
  public isUpdate = false;

  securityObject: any;
  public availableBalance = 0;
  public leaveBalanceError: boolean;
  public type: any;
  public leaveBalance: any;

  public today: Date = new Date();
  public minDate: Object = new Date(this.today.getFullYear()-1,1,null);
  public maxDate: Object =  new Date(this.today.getFullYear(),12,null);

  constructor(
    private service: LeaveService,
    private fb?: FormBuilder,
    private router?: Router,
    private location?: Location,
    private actRoute?: ActivatedRoute
  ) {
    this.formGroup = this.fb.group({
      id: [''],
      from: ['', Validators.required],
      number_of_days: ['', Validators.required],
      type: ['', Validators.required],
      comment: [''],
      is_loan: [false]
    });

    if (localStorage.getItem('selfServiceAccessToken')) {
      this.securityObject = JSON.parse(
        localStorage.getItem('selfServiceAccessToken')
      );
    }
  }

  ngOnInit() {
    this.employeeId = this.securityObject.employee_id;
    this.load_leave_type({ emp_id: this.securityObject.employee_id });
    this.ID = this.actRoute.snapshot.params.id;
    if (this.ID !== undefined) {
      this.isUpdate = true;
      this.service.get(this.ID).subscribe((data: any) => {
        data.step > 1 ? this.formGroup.disable() : null;
        data.is_loan === null ? data.is_loan = false : data.is_loan = true;
        delete data.step;
        this.initializeForm(data);
      });
    }
  }

  initializeForm(data) {
    this.formGroup = this.fb.group({
      id: [data.id],
      from: [data.from, Validators.required],
      number_of_days: [data.number_of_days, Validators.required],
      type: [data.type, Validators.required],
      comment: [data.comment],
      is_loan: [data.is_loan]
    });
  }

  Submit(): any {
    if (!this.formGroup.valid) {
      this.formSubmitted = true;
      return;
    } else {
      if (this.remainingBalance < this.formGroup.controls.number_of_days.value) {
        this.formGroup.controls.number_of_days.reset();
        this.notif.Show({title: 'Error',content: 'Invalid Amount.',cssClass: 'e-toast-info'});
        return;
      }
      if (this.isUpdate) {
        return this.service
          .update({
            requested_by: this.securityObject.employee_id,
            emp_id:
              this.reqforOther.canRequestForOther && this.reqforOther.checked
                ? this.reqforOther.employeeReqId
                : this.securityObject.employee_id,
            data: JSON.stringify(this.formGroup.value)
          })
          .subscribe((response: any) => {
            if (response.status === true) {
              this.router.navigate(['/workspace/leave'], {
                state: {
                  data: {
                    title: 'Success',
                    content: response.message,
                    cssClass: 'e-toast-success'
                  }
                }
              });
            } else {
              this.notif.Show({
                title: 'Error',
                content: response.message,
                cssClass: 'e-toast-danger'
              });
            }
          });
      } else {
        return this.service
          .create({
            requested_by: this.securityObject.employee_id,
            emp_id:
              this.reqforOther.canRequestForOther && this.reqforOther.checked
                ? this.reqforOther.employeeReqId
                : this.securityObject.employee_id,
            action_id: '5',
            data: JSON.stringify(this.formGroup.value)
          })
          .subscribe((response: any) => {
            if (response.status === true) {
              this.router.navigate(['/workspace/leave'], {
                state: {
                  data: {
                    title: 'Success',
                    content: response.message,
                    cssClass: 'e-toast-success'
                  }
                }
              });
            } else {
              this.notif.Show({
                title: 'Error',
                content: response.message,
                cssClass: 'e-toast-danger'
              });
            }
          });
      }
    }
  }

  cancel() {
    this.location.back();
  }

  setEmployee(employeeId) {
    this.employeeId = employeeId;
    this.getBalance(this.formGroup.controls.type.value);
  }

  load_leave_type(emp_id) {
    this.service.loadLeaveType(emp_id).subscribe((data: any) => {
      this.type = data;
    });
  }

  getLeaveType($event) {
    this.service.loadLeaveType({ emp_id: $event }).subscribe((data: any) => {
      this.type = data;
    });
  }

  getBalance(id) {
    this.type.forEach(t => {
        if (t.id.indexOf(id) > -1) {
          t.balance === null ? this.remainingBalance = 0 : this.remainingBalance = t.balance;
        }
      });
  }

  applyForLoan($event){
    $event.checked ? this.remainingBalance = 30 : this.remainingBalance = 0;
  }


}
