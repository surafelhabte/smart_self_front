import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/shared/shared.module';
import { WorkspaceComponent } from './workspace/workspace.component';
import { HttpClientModule } from '@angular/common/http';
import { RequestManagementModule } from './request-management/request-management.module';
import { NotificationManagementModule } from './notification-management/notification-management.module';
import { ClickOutsideModule } from 'ng-click-outside';

const routes: Routes = [
  {
   path: 'workspace',
   component: WorkspaceComponent,
   children: [
    {
      path: '',
      loadChildren: () =>
        import(
          '../featured/dashboard-management/dashboard-management.module'
        ).then(m => m.DashboardManagementModule)
    },
    {
      path: 'medical-reimbursement',
      loadChildren: () =>
        import(
          '../featured/medical-reimbursement-management/medical-reimbursement-management.module'
        ).then(m => m.MedicalReimbursementManagementModule)
    },
    {
      path: 'salary-advance',
      loadChildren: () =>
        import(
          '../featured/salary-advance-management/salary-advance-management.module'
        ).then(m => m.SalaryAdvanceManagementModule)
    },
    {
      path: 'leave',
      loadChildren: () =>
        import('../featured/leave-management/leave-management.module').then(
          m => m.LeaveManagementModule
        )
    },
    {
      path: 'acting-assignment',
      loadChildren: () =>
        import(
          '../featured/acting-assignment-management/acting-assignment-management.module'
        ).then(m => m.ActingAssignmentManagementModule)
    },
    {
      path: 'termination',
      loadChildren: () =>
        import(
          '../featured/termination-management/termination-management.module'
        ).then(m => m.TerminationManagementModule)
    },
    {
      path: 'work-experience',
      loadChildren: () =>
        import('../featured/work-experience/work-experience.module').then(
          m => m.WorkExperienceModule
        )
    },
    {
      path: 'news',
      loadChildren: () => import('./news/news.module').then(m => m.NewsModule)
    },
    {
      path: 'requests',
      loadChildren: () =>
        import('./request-management/request-management.module').then(
          m => m.RequestManagementModule
        )
    },
    {
      path: 'notification',
      loadChildren: () =>
        import(
          '../featured/notification-management/notification-management.module'
        ).then(m => m.NotificationManagementModule)
    },
    {
      path: 'activity-workflow',
      loadChildren: () =>
        import('../featured/activity-workflow/activity-workflow.module').then(
          m => m.ActivityWorkflowModule
        ),
      data: { title: 'Activity workflow' }
    },
    {
      path: 'attendance-report',
      loadChildren: () =>
        import(
          '../featured/attendance-management/attendance-management.module'
        ).then(m => m.AttendanceManagementModule),
      data: { title: 'Attendance report' }
    },
    {
      path: 'vacancy',
      loadChildren: () =>
        import('../featured/vacancy/vacancy.module').then(m => m.VacancyModule),data: { title: 'Vacancy' }
    }
   ]  
  },
 
];

@NgModule({
  imports: [
    CommonModule, 
    RouterModule.forChild(routes), 
    SharedModule,  
    HttpClientModule,
    RequestManagementModule,
    NotificationManagementModule,
    ClickOutsideModule
  ],
  exports: [SharedModule],
  declarations: [WorkspaceComponent],
  providers: []
})
export class FeaturedModule {}
