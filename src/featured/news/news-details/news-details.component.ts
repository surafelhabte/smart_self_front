import { Component, OnInit } from '@angular/core';
import { NewsService } from '../news.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';


@Component({
  selector: 'app-news-details',
  templateUrl: './news-details.component.html',
  styleUrls: ['./news-details.component.css']
})
export class NewsDetailsComponent implements OnInit {

  public news: any;
  public ID: any;

  constructor(private service: NewsService, private location?: Location, private actRoute?: ActivatedRoute) {}

  ngOnInit() {
    this.ID = this.actRoute.snapshot.params.id;
    if (this.ID !== undefined) {
      this.service.details(this.ID).subscribe((data: any) => {
        this.news = data;
      });
    }
  }

  back() {
    this.location.back();
  }

}
