import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  private url = environment.baseUrl + 'dashboard/dashboard';

  constructor(private http?: HttpClient) {}

  details(id: any) {
    return this.http.get(this.url + '/GetNewsDetail/' + id);
  }

  get() {
    return this.http.get(this.url + '/GetNews');
  }
}
