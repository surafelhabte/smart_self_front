import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsDetailsComponent } from './news-details/news-details.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/shared/shared.module';
import { HtmlSanitaizerModule } from './htmlSanitaizer/html-sanitaizer.module';

const routes: Routes = [
  {
    path: 'detail/:id',
    component: NewsDetailsComponent,
    data: { title: 'News' }
  }
];

@NgModule({
  declarations: [NewsDetailsComponent],
  imports: [
    CommonModule,
    SharedModule,
    HtmlSanitaizerModule,
    RouterModule.forChild(routes)
  ]
})
export class NewsModule {}
