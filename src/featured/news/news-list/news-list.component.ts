import { Component, OnInit } from '@angular/core';
import { NewsService } from '../news.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.css']
})
export class NewsListComponent implements OnInit {

  public news: any;

  constructor(private service: NewsService, private route: Router) {}

  ngOnInit() {
    this.service.get().subscribe((data: any) => {
      this.news = data;
    });
  }
}
