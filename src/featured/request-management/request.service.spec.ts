/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import {
  HttpTestingController,
  HttpClientTestingModule
} from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { RequestService } from './request.service';

describe('Service: Request', () => {
  let nService: RequestService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule]
    });

    nService = TestBed.get(RequestService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('Should get and list requests ', () => {
    const loan = [
      {
        id: 1,
        requestType: 'Medical reimbursement',
        requestedOn: '2020-02-28',
        requestedBy: 'Surafel habte'
      }
    ];

    nService.getRequests({ empId: 1 }).subscribe((data: any) => {
      expect(data).toBe(loan);
    });
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');
    req.flush(loan);
    httpMock.verify();
  });

  it('Should get and list notifications ', () => {
    const loan = [
      {
        id: 1,
        notifType: 'Medical reimbursement',
        updatedOn: '2020-02-28',
        status: true
      }
    ];

    nService.getRequests({ empId: 1 }).subscribe((data: any) => {
      expect(data).toBe(loan);
    });
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');
    req.flush(loan);
    httpMock.verify();
  });
});
