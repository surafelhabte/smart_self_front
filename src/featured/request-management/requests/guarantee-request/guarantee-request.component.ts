import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import {
  DialogComponent,
  PositionDataModel
} from "@syncfusion/ej2-angular-popups";
import { SharedToastComponent } from "src/shared/shared-toast/shared-toast.component";
import { Router, ActivatedRoute } from "@angular/router";
import { EmitType } from "@syncfusion/ej2-base";
import { RequestService } from "../../request.service";
import { Location } from "@angular/common";

@Component({
  selector: "app-guarantee-request",
  templateUrl: "./guarantee-request.component.html",
  styleUrls: ["./guarantee-request.component.css"]
})
export class GuaranteeRequestComponent implements OnInit {
  @ViewChild("template") template: DialogComponent;
  @ViewChild("container", { read: ElementRef }) container: ElementRef;
  @ViewChild("notif") notif: SharedToastComponent;
  requestId;
  public employeeId: string;
  public employeeInfo: any;
  public updatedDate: any;
  public position: PositionDataModel = { X: "center", Y: "center" };
  public closeOnEscape = false;
  public dialogCloseIcon = true;
  public defaultWidth = "452px";
  public target = ".control-section";
  public targetElement: HTMLElement;
  public proxy: any = this;
  public urlModule = "loan";
  public requestDetail: any;
  public totalExpenses = 0;
  securityObject: any;
  constructor(
    private apiService: RequestService,
    private router: Router,
    private location: Location,
    private activatedRoute: ActivatedRoute
  ) {
    this.employeeId = this.apiService.employeeId;
    this.updatedDate = this.apiService.updatedDate;
    if (localStorage.getItem("selfServiceAccessToken")) {
      this.securityObject = JSON.parse(
        localStorage.getItem("selfServiceAccessToken")
      );
    }
  }

  ngOnInit() {
    this.apiService
      .getEmployeeInfo({ emp_id: this.employeeId })
      .subscribe((data: any) => {
        this.employeeInfo = data;
      });

    this.requestId = this.activatedRoute.snapshot.paramMap.get("id");
    this.apiService
      .getRequest(this.requestId, this.urlModule)
      .subscribe((data: any) => {
        this.requestDetail = data;
      });
  }
  public dialogOpen: EmitType<object> = () => {
    document.getElementById("approve").onclick = (): void => {
      this.approve();
    };
    document.getElementById("reject").onclick = (): void => {
      this.reject();
    };
  };

  public dialogClose: EmitType<object> = () => {
    this.location.back();
  };

  approve() {
    const reqInfo = {
      loan_id: this.requestDetail.id,
      emp_id: this.securityObject.employee_id,
      status: "approve"
    };
    const conf = confirm("Are you sure you want to accept this request ?");
    if (conf) {
      this.apiService.updateGuaranteeStatus(reqInfo).subscribe((data: any) => {
        if (data.status) {
          this.notif.Show({
            title: "Success",
            content: data.message,
            cssClass: "e-toast-success"
          });
        } else {
          this.notif.Show({
            title: "Error",
            content: data.message,
            cssClass: "e-toast-danger"
          });
        }
      });
    }
  }

  reject() {
    const reqInfo = {
      id: this.requestId,
      status: "reject"
    };
    const conf = confirm("Are you sure you want to reject this request ?");
    if (conf) {
      this.apiService.updateRequestStatus(reqInfo).subscribe((data: any) => {
        if (data.status) {
          this.notif.Show({
            title: "Success",
            content: data.message,
            cssClass: "e-toast-success"
          });
        } else {
          this.notif.Show({
            title: "Error",
            content: data.message,
            cssClass: "e-toast-danger"
          });
        }
      });
    }
  }
}
