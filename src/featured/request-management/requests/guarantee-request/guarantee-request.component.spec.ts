import {
  ComponentFixture,
  TestModuleMetadata,
  TestBed,
  async
} from "@angular/core/testing";
import { DebugElement } from "@angular/core";
import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { SharedModule } from "src/shared/shared.module";
import { RequestService } from "../../request.service";
import { GuaranteeRequestComponent } from "./guarantee-request.component";

describe("GuaranteeRequestComponent", () => {
  let component: GuaranteeRequestComponent;
  let fixture: ComponentFixture<GuaranteeRequestComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [RouterTestingModule, HttpClientTestingModule, SharedModule],
      providers: [RequestService],
      declarations: [GuaranteeRequestComponent]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(GuaranteeRequestComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
