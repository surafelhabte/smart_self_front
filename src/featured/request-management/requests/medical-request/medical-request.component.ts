import {
  Component,
  OnInit,
  Input,
  ViewChild,
  ElementRef,
  AfterViewInit
} from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { RequestService } from "../../request.service";
import {
  DialogComponent,
  PositionDataModel
} from "@syncfusion/ej2-angular-popups";
import { SharedToastComponent } from "src/shared/shared-toast/shared-toast.component";
import { EmitType } from "@syncfusion/ej2-base";
import { Location } from "@angular/common";

@Component({
  selector: "app-medical-request",
  templateUrl: "./medical-request.component.html",
  styleUrls: ["./medical-request.component.css"]
})
export class MedicalRequestComponent implements OnInit {
  @ViewChild("template") template: DialogComponent;
  @ViewChild("container", { read: ElementRef }) container: ElementRef;
  @ViewChild("notif") notif: SharedToastComponent;
  requestId;
  public employeeId: string;
  public updatedDate: any;
  public employeeInfo: any;
  public position: PositionDataModel = { X: "center", Y: "center" };
  public closeOnEscape = false;
  public dialogCloseIcon = true;
  public defaultWidth = "452px";
  public target = ".control-section";
  public targetElement: HTMLElement;
  promptHeader = "Login";
  public proxy: any = this;
  public urlModule = "medical";
  public requestDetail: any;
  public totalExpenses = 0;
  public dlgButtons: Object[] = [
    {
      click: this.dlgBtnClick.bind(this),
      buttonModel: { content: "Yes", isPrimary: "true" }
    },
    { click: this.dlgBtnClick.bind(this), buttonModel: { content: "No" } }
  ];
  dlgBtnClick() {
    alert("yess");
  }
  constructor(
    public apiService: RequestService,
    private router: Router,
    private location: Location,
    private activatedRoute: ActivatedRoute
  ) {
    this.employeeId = this.apiService.employeeId;
    this.updatedDate = this.apiService.updatedDate;
  }

  ngOnInit() {
    this.apiService
      .getEmployeeInfo({ emp_id: this.employeeId })
      .subscribe((data: any) => {
        this.employeeInfo = data;
      });

    this.requestId = this.activatedRoute.snapshot.paramMap.get("id");
    this.apiService
      .getRequest(this.requestId, this.urlModule)
      .subscribe((data: any) => {
        this.requestDetail = data;
        if (this.requestDetail) {
          this.requestDetail.data.forEach(element => {
            this.totalExpenses = this.totalExpenses + +element.amount;
          });
        }
      });
  }

  public dialogOpen: EmitType<object> = () => {
    document.getElementById("approve").onclick = (): void => {
      this.approve();
    };
    document.getElementById("reject").onclick = (): void => {
      this.reject();
    };
  };

  public dialogClose: EmitType<object> = () => {
    this.location.back();
  };

  approve() {
    const reqInfo = {
      id: this.requestId,
      status: "approve"
    };
    const conf = confirm("Are you sure you want to approve this request ?");
    if (conf) {
      this.apiService.updateRequestStatus(reqInfo).subscribe((data: any) => {
        if (data.status) {
          this.notif.Show({
            title: "Success",
            content: data.message,
            cssClass: "e-toast-success"
          });
        } else {
          this.notif.Show({
            title: "Error",
            content: data.message,
            cssClass: "e-toast-danger"
          });
        }
      });
    }
  }

  reject() {
    const reqInfo = {
      id: this.requestId,
      status: "reject"
    };
    const conf = confirm("Are you sure you want to reject this request ?");
    if (conf) {
      this.apiService.updateRequestStatus(reqInfo).subscribe((data: any) => {
        if (data.status) {
          this.notif.Show({
            title: "Success",
            content: data.message,
            cssClass: "e-toast-success"
          });
        } else {
          this.notif.Show({
            title: "Error",
            content: data.message,
            cssClass: "e-toast-danger"
          });
        }
      });
    }
  }
  
}
