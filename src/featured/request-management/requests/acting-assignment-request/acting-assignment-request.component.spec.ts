import {
  ComponentFixture,
  TestModuleMetadata,
  TestBed,
  async
} from "@angular/core/testing";
import { DebugElement } from "@angular/core";
import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { SharedModule } from "src/shared/shared.module";
import { ActingAssignmentRequestComponent } from "./acting-assignment-request.component";
import { RequestService } from "../../request.service";

describe("ActingAssignmentRequestComponent", () => {
  let component: ActingAssignmentRequestComponent;
  let fixture: ComponentFixture<ActingAssignmentRequestComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [RouterTestingModule, HttpClientTestingModule, SharedModule],
      providers: [RequestService],
      declarations: [ActingAssignmentRequestComponent]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(ActingAssignmentRequestComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
