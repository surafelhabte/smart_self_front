import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RequestService } from '../request.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-request-list',
  templateUrl: './request-list.component.html',
  styleUrls: ['./request-list.component.css']
})
export class RequestListComponent implements OnInit {
  public requests = [];

  public securityObject: any;
  @Output() public total_request: EventEmitter<any> = new EventEmitter();

 
  constructor(private apiService: RequestService, private router: Router) {
    if (localStorage.getItem('selfServiceAccessToken')) {
      this.securityObject = JSON.parse(localStorage.getItem('selfServiceAccessToken'));
    }
  }

  ngOnInit() {
    this.apiService
      .getRequests({ position_id: this.securityObject.position_id })
      .subscribe((data: any[]) => {
        this.requests = data;
      });

    this.apiService
      .getGuarantorRequest({ emp_id: this.securityObject.employee_id })
      .subscribe((data: any[]) => {
        this.requests = this.requests.concat(data);
      });

      if(this.requests.length > 0){
        this.total_request.emit(this.requests);
      }
  }

  viewRequestDetail(requestId, requestType, empId, updatedDate) {
    this.apiService.employeeId = empId;
    this.apiService.updatedDate = updatedDate;
    this.router.navigate(['workspace/requests/' + requestType + '/' + requestId]);
  }
 
}
