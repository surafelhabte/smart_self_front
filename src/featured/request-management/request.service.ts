import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RequestService {
  public employeeId: string;
  public updatedDate: any;
  public url = environment.baseUrl;

  constructor(private http: HttpClient) {}

  getRequests(emp_info) {
    return this.http.post(this.url + 'request/request/get/', emp_info);
  }

  getRequest(id: string, urlModule) {
    return this.http.get(`${this.url + urlModule}/${urlModule}/get/${id}`);
  }

  getEmployeeInfo(empId: any) {
    return this.http.post(this.url + 'employee/employee/get', empId);
  }

  updateRequestStatus(requestInfo: object) {
    return this.http.post(this.url + 'request/request/update/', requestInfo);
  }

  updateGuaranteeStatus(requestInfo: object) {
    return this.http.post(this.url + 'loan/loan/updateGuarantee/', requestInfo);
  }

  getGuarantorRequest(empId) {
    return this.http.post(
      this.url + 'request/request/GetGuaranteeRequest/',
      empId
    );
  }
}
