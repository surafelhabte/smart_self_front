import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MedicalRequestComponent } from './requests/medical-request/medical-request.component';
import { WorkExperienceRequestComponent } from './requests/work-experience-request/work-experience-request.component';
import { TerminationRequestComponent } from './requests/termination-request/termination-request.component';
import { SalaryAdvanceRequestComponent } from './requests/salary-advance-request/salary-advance-request.component';
import { LeaveRequestComponent } from './requests/leave-request/leave-request.component';
import { SharedModule } from 'src/shared/shared.module';
import { ActingAssignmentRequestComponent } from './requests/acting-assignment-request/acting-assignment-request.component';
import { RequestListComponent } from './request-list/request-list.component';
import { GuaranteeRequestComponent } from './requests/guarantee-request/guarantee-request.component';

const routes: Routes = [
  {
    path: 'list',
    component: RequestListComponent,
    data: { title: 'Requests' }
  },
  {
    path: 'medical-reimbursement/:id',
    component: MedicalRequestComponent
  },
  {
    path: 'salary-advance/:id',
    component: SalaryAdvanceRequestComponent
  },
  {
    path: 'leave/:id',
    component: LeaveRequestComponent
  },
  {
    path: 'work-experience/:id',
    component: WorkExperienceRequestComponent
  },
  {
    path: 'acting-assignment/:id',
    component: ActingAssignmentRequestComponent
  },
  {
    path: 'termination/:id',
    component: TerminationRequestComponent
  },
  {
    path: 'guarantor/:id',
    component: GuaranteeRequestComponent
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), SharedModule],
  declarations: [
    RequestListComponent,
    MedicalRequestComponent,
    WorkExperienceRequestComponent,
    TerminationRequestComponent,
    SalaryAdvanceRequestComponent,
    LeaveRequestComponent,
    ActingAssignmentRequestComponent,
    GuaranteeRequestComponent
  ],
  exports: [RequestListComponent]
})
export class RequestManagementModule {}
