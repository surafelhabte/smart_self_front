import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WorkSpaceService {
  public totalRequest = 0;
  public totalNotification = 0;
  private url = environment.baseUrl;

  constructor(private http: HttpClient) {}

  getMenus() {
    return this.http.get(this.url + 'action/action/gets');
  }
}
