import { Component, OnInit, ViewChild } from '@angular/core';
import { WorkSpaceService } from './workspace.service';
import { Router, NavigationEnd } from '@angular/router';
import { RequestService } from '../request-management/request.service';
import { NotificationService } from '../notification-management/notification.service';
import { SecurityService, AppUserAuth } from 'src/core/security.service';
import { NodeSelectEventArgs, TreeViewComponent, SidebarComponent } from '@syncfusion/ej2-angular-navigations';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';

@Component({
  selector: 'app-workspace',
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.css']
})
export class WorkspaceComponent implements OnInit {
  @ViewChild('notif') notif: SharedToastComponent;
  @ViewChild('sidebar') sidebar: SidebarComponent;
  @ViewChild('tree') tree: TreeViewComponent;
  viewNotification = false;
  viewRequest = false;
  position: string;
  fullName: string;
  empId: string;
  isLoggedIn = false;
  totalRequest = 0;
  totalNotification = 0;
  field: object;
  public securityObject: AppUserAuth;
  public roles: any;
  
  public navigations = [
    {
      id: '1',
      name: 'Dashboard',
      expanded: 'false',
      enabled: 'true',
      url: '/workspace',
      selected: 'false',
      icon: 'fas fa-columns',
      subChild: [],
      rolename: 'viewDashboard'
    },
    {
      id: '2',
      name: 'Salary Advance',
      expanded: 'false',
      enabled: 'true',
      url: '/workspace/salary-advance',
      selected: 'false',
      icon: 'fas fa-hand-holding-usd',
      subChild: [],
      rolename: 'viewSalaryAdvance'
    },
    {
      id: '3',
      name: 'Medical reimbursement',
      expanded: 'false',
      enabled: 'true',
      url: '/workspace/medical-reimbursement',
      selected: 'false',
      icon: 'fas fa-procedures',
      subChild: [],
      rolename: 'viewMedical'
    },
    {
      id: '4',
      name: 'Work Experience',
      expanded: 'false',
      enabled: 'true',
      url: '/workspace/work-experience',
      selected: 'false',
      icon: 'fas fa-people-carry',
      subChild: [],
      rolename: 'viewExperience'
    },
    {
      id: '5',
      name: 'Leave',
      expanded: 'false',
      enabled: 'true',
      url: '/workspace/leave',
      selected: 'false',
      icon: 'fas fa-location-arrow',
      subChild: [],
      rolename: 'viewLeave'
    },
    {
      id: '10',
      name: 'Schedule Leave',
      expanded: 'false',
      enabled: 'true',
      url: '/workspace/leave/schedule',
      selected: 'false',
      icon: 'fas fa-location-arrow',
      subChild: [],
      rolename: 'viewLeave'
    },
    {
      id: '6',
      name: 'Resignation',
      expanded: 'false',
      enabled: 'true',
      url: '/workspace/termination',
      selected: 'false',
      icon: 'fas fa-sign-out-alt',
      subChild: [],
      rolename: 'viewTermination'
    },
    {
      id: '7',
      name: 'Activity Work Flow',
      expanded: 'false',
      enabled: 'true',
      url: '/workspace/activity-workflow',
      selected: 'false',
      icon: 'fab fa-dropbox',
      subChild: [],
      rolename: 'viewActivityWorkflow'
    },
    {
      id: '8',
      name: 'Acting Assignment',
      expanded: 'false',
      enabled: 'true',
      url: '/workspace/acting-assignment',
      selected: 'false',
      icon: 'fas fa-users-cog',
      subChild: [],
      rolename: 'viewActingAssignment'
    },
    {
      id: '9',
      name: 'Attendance',
      expanded: 'false',
      enabled: 'true',
      url: '/workspace/attendance-report',
      selected: 'false',
      icon: 'fas fa-stream',
      subChild: [],
      rolename: 'viewAttendance'
    },
    {
      id: '11',
      name: 'Internal Vacancy',
      expanded: 'false',
      enabled: 'true',
      url: '/workspace/vacancy',
      selected: 'false',
      icon: 'fas fa-stream',
      subChild: [],
      rolename: 'viewVacancy'
    }
  ];

  constructor(public router: Router,public apiService: WorkSpaceService,public securityService: SecurityService,
    public reqService: RequestService,public notfService: NotificationService) {
    this.securityObject = new AppUserAuth();
    if (localStorage.getItem('selfServiceAccessToken')) {
      this.securityObject = JSON.parse(localStorage.getItem('selfServiceAccessToken'));
    }
    this.field = {
      dataSource: this.navigations,
      id: 'id',
      text: 'name',
      child: 'subChild',
      expanded: 'expanded',
      selected: 'selected',
      enabled: 'enabled'
    };

    for (let index = 1; index < this.navigations.length; index++) {
      const subChild = this.navigations[index].subChild;
      if (subChild.length === 0) {
        if (!this.securityService.hasClaim(this.navigations[index].rolename)) {
          this.navigations.splice(index, 1);
          --index;
        }
      }
    }
  }

  ngOnInit(): void {
    this.fullName = this.securityObject.full_name;
    this.position = this.securityObject.position;
    this.empId = this.securityObject.employee_id;

    this.securityObject = this.securityService.securityObject;
    this.roles = this.securityObject.roles.filter(elem => elem.status === 'true');

    this.apiService.getMenus().subscribe(() => {
      // this.navigations = data;
    });

    this.reqService
      .getRequests({ position_id: this.securityObject.position_id })
      .subscribe((data: any[]) => {
        this.totalRequest = this.totalRequest + data.length;
      });

    this.reqService
      .getGuarantorRequest({ emp_id: this.securityObject.employee_id })
      .subscribe((data: any) => {
        if (data.length > 0) {
          this.totalRequest = this.totalRequest + data.length;
        }
      });

    this.notfService
      .getNotification({ emp_id: this.empId })
      .subscribe((data: any[]) => {
        this.totalNotification = data.length;
      });
    if (!this.securityObject.key) {
      this.isLoggedIn = false;
      this.router.navigate(['login']);
    } else {
      this.isLoggedIn = true;
    }

    this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        if (history.state.data !== undefined) {
          this.notif.Show(history.state.data);
        }
      }
    });
  }

  public onCreated() {
    this.sidebar.element.style.visibility = '';
  }

  public loadRoutingContent(args: NodeSelectEventArgs): void {
    const data: any = this.tree.getTreeData(args.node);
    const routerLink: string = data[0].url;

    if (routerLink !== 'parent') {
      this.router.navigate([routerLink]);
    }
  }

  logout() {
    this.securityService.logOut();
  }

  getNotification() {
    const item = document.getElementsByClassName('popover-notification') as HTMLCollectionOf<HTMLElement>;
    const item2 = document.getElementsByClassName('popover-request') as HTMLCollectionOf<HTMLElement>;
    item2[0].style.display = 'none';
    if (!this.viewNotification) {
      item[0].style.display = 'block';
      this.viewNotification = !this.viewNotification;
    } else {
      item[0].style.display = 'none';
      this.viewNotification = !this.viewNotification;
    }
  }

  getRequest() {
    const item = document.getElementsByClassName('popover-request') as HTMLCollectionOf<HTMLElement>;
    const item2 = document.getElementsByClassName('popover-notification') as HTMLCollectionOf<HTMLElement>;
    item2[0].style.display = 'none';
    if (!this.viewRequest) {
      item[0].style.display = 'block';
      this.viewRequest = !this.viewRequest;
    } else {
      item[0].style.display = 'none';
      this.viewRequest = !this.viewRequest;
    }
  }

  getAllRequests() {
    const item = document.getElementsByClassName('popover-notification') as HTMLCollectionOf<HTMLElement>;
    item[0].style.display = 'none';
    this.router.navigate(['workspace/requests/list']);
  }

  getAllNotifications() {
    const item = document.getElementsByClassName('popover-request') as HTMLCollectionOf<HTMLElement>;
    item[0].style.display = 'none';
    this.router.navigate(['workspace/notification']);
  }

  onClickedOutsideRequest() {
    const req = document.getElementsByClassName('popover-request') as HTMLCollectionOf<HTMLElement>;
    if (this.viewRequest) {
      req[0].style.display = 'none';
      this.viewRequest = !this.viewRequest;
    }
  }

  onClickedOutsideNotification() {
    const notf = document.getElementsByClassName('popover-notification') as HTMLCollectionOf<HTMLElement>;
    if (this.viewNotification) {
      notf[0].style.display = 'none';
      this.viewNotification = !this.viewNotification;
    }
  }

  total_request($event){
    this.totalRequest = $event;
  }

}
