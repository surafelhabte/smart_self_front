import { AuthService } from './auth.service';
import {
  HttpTestingController,
  HttpClientTestingModule
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { LoginModel } from '../login-model';

describe('Auth service', () => {
  let authService: AuthService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule]
    });

    authService = TestBed.get(AuthService);
    httpMock = TestBed.get(HttpTestingController);
  });
  it('Should check account successfully', () => {
    const account: LoginModel = {
      username: 'appdiv',
      password: 'systems'
    };

    authService
      .login(account.username, account.password)
      .subscribe((data: any) => {
        expect(data).toBe(account);
      });
    // telling the httmock what kind of request we expect and toward which url
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');

    // fire the request with its data we really expect

    req.flush(account);

    httpMock.verify();
  });
});
