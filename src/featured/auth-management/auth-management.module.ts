import { NgModule } from '@angular/core';
import { AuthManagementComponent } from './auth-management.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: AuthManagementComponent
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
  ],
  declarations: [AuthManagementComponent]
})
export class AuthManagementModule {}
