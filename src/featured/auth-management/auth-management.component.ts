import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit
} from '@angular/core';
import {
  DialogComponent,
  PositionDataModel
} from '@syncfusion/ej2-angular-popups';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from '@angular/forms';
import { LoginModel } from './login-model';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';
import { EmitType } from '@syncfusion/ej2-base';
import { SecurityService } from 'src/core/security.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth-management',
  templateUrl: './auth-management.component.html',
  styleUrls: ['./auth-management.component.css']
})
export class AuthManagementComponent implements OnInit {

  constructor(
    private elementRef: ElementRef,
    private formBuilder: FormBuilder,
    private securityService: SecurityService,
    private route: Router
  ) {
    if (localStorage.getItem('selfServiceAccessToken')) {
      this.securityObject = JSON.parse(
        localStorage.getItem('selfServiceAccessToken')
      );
      if (this.securityObject.key) {
        this.route.navigate(['']);
      }
    }
    this.createForm();
  }

  get username(): FormControl {
    return this.loginForm.get('username') as FormControl;
  }

  get password(): FormControl {
    return this.loginForm.get('password') as FormControl;
  }
  @ViewChild('template') template: DialogComponent;
  @ViewChild('container', { read: ElementRef }) container: ElementRef;
  @ViewChild('notif') notif: SharedToastComponent;
  public position: PositionDataModel = { X: 'center', Y: 'center' };
  public defaultWidth = '500px';
  public target = '.control-section';
  public targetElement: HTMLElement;
  promptHeader = 'Login';
  public proxy: any = this;
  public dlgButtons: any[] = [
    {
      click: this.dlgBtnClick.bind(this),
      buttonModel: { content: 'Yes', isPrimary: 'true' }
    },
    { click: this.dlgBtnClick.bind(this), buttonModel: { content: 'No' } }
  ];
  securityObject: any;

  loginForm: FormGroup;
  submitClicked = false;
  dlgBtnClick() {
    alert('yess');
  }

  ngOnInit() {}
  createForm(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  prepareFormData(): LoginModel | null {
    if (this.loginForm.valid) {
      return {
        username: this.username.value,
        password: this.password.value
      };
    } else {
      return null;
    }
  }

  submit() {
    this.submitClicked = true;
    const payload = this.prepareFormData();
    if (payload) {
      this.securityService
        .logIn(this.username.value, this.password.value)
        .subscribe();
    }
  }

  public dialogOpen: EmitType<object> = () => {
    document.getElementById('login').onclick = (): void => {
      this.submit();
    };
  }
}
