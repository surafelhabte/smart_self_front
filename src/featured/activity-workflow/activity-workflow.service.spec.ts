/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ActivityWorkflowService } from './activity-workflow.service';
import {
  HttpTestingController,
  HttpClientTestingModule
} from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivityWorkflow } from './activity-workflow';

describe('Service: ActivityWorkflow', () => {
  let wService: ActivityWorkflowService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule]
    });

    wService = TestBed.get(ActivityWorkflowService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('Should get and list activity workflows ', () => {
    const workflow = [
      {
        id: '2',
        name: 'Medical Refund',
        step: '1',
        authorized_by: 'Manager'
      },
      {
        id: '3',
        name: 'Medical Refund',
        step: '2',
        authorized_by: 'Officer'
      }
    ];

    wService.getWorkflows().subscribe((data: any) => {
      expect(data).toBe(workflow);
    });
    // telling the httmock what kind of request we expect and toward which url
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('GET');

    // fire the request with its data we really expect

    req.flush(workflow);

    httpMock.verify();
  });
  it('Should create activity work flow', () => {
    const activity: ActivityWorkflow = {
      action_id: '1',
      job_grade_lower: 1,
      job_grade_higher: 3,
      workflow: [
        { step: 1, authorized_by: 'Manager' },
        { step: 2, authorized_by: 'Officers' }
      ]
    };
    wService.create(activity).subscribe((data: any) => {
      expect(data.workflow.length).toBeGreaterThan(0);
    });
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');
    req.flush(activity);
    httpMock.verify();
  });

  it('Should get activity work flow successfull', () => {
    const activity: ActivityWorkflow = {
      action_id: '2',
      job_grade_lower: 1,
      job_grade_higher: 3,
      workflow: [
        { step: 1, authorized_by: 'Manager' },
        { step: 2, authorized_by: 'Officers' }
      ]
    };

    wService.getWorkflow(activity.action_id).subscribe((data: any) => {
      expect(data).toBe(activity);
    });
    // telling the httmock what kind of request we expect and toward which url
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('GET');

    // fire the request with its data we really expect

    req.flush(activity);

    httpMock.verify();
  });

  it('Should update activity workflow', () => {
    const activity: ActivityWorkflow = {
      action_id: '1',
      job_grade_lower: 1,
      job_grade_higher: 3,
      workflow: [
        { step: 1, authorized_by: 'Manager' },
        { step: 2, authorized_by: 'Officers' }
      ]
    };

    wService.update(activity).subscribe((data: any) => {
      expect(data.action_id).toBe('1');
    });
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');

    req.flush(activity);

    httpMock.verify();
  });

  it('Should delete activity overflow', () => {
    wService.deleteActivityWorkflow(12).subscribe((data: any) => {
      expect(data).toBe(12);
    });

    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('DELETE');

    req.flush(12);

    httpMock.verify();
  });
});
