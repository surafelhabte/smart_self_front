import { ActivityWorkflowFormComponent } from './activity-workflow-form.component';
import {
  ComponentFixture,
  TestModuleMetadata,
  TestBed,
  async
} from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from 'src/shared/shared.module';
import { ActivityWorkflowService } from '../activity-workflow.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

const crypto = require('crypto');
Object.defineProperty(global, 'crypto', {
  value: {
    getRandomValues: arr => crypto.randomBytes(arr.length)
  }
});

describe('ActivityWorkflowFormComponent', () => {
  let component: ActivityWorkflowFormComponent;
  let fixture: ComponentFixture<ActivityWorkflowFormComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [SharedModule, RouterTestingModule, HttpClientTestingModule],
      declarations: [ActivityWorkflowFormComponent],
      providers: [ActivityWorkflowService]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(ActivityWorkflowFormComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('on submit', () => {
    it('Submit should be called', () => {
      const submitFun = spyOn(component, 'onSubmit');
      component.onSubmit();
      expect(submitFun).toHaveBeenCalled();
    });
  });

  describe('Requirment form', () => {
    it('Should be created', () => {
      const obj1 = {
        createForm: () => null
      };
      spyOn(obj1, 'createForm');
      obj1.createForm();
      expect(obj1.createForm).toBeTruthy();
    });

    it('Should be invalid when empty', () => {
      expect(component.workflowForm.valid).toBeFalsy();
    });
    it('requirment fields validity sample for action field', () => {
      const reqField = component.action;
      expect(reqField.valid).toBeFalsy();
      reqField.setValue('action');
      expect(reqField.valid).toBeTruthy();
    });
  });
});
