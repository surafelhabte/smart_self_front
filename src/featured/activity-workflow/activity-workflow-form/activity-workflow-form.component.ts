import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormArray,
  FormControl
} from '@angular/forms';
import { ActivityWorkflowService } from '../activity-workflow.service';
import { Location } from '@angular/common';
import { TreeView } from '@syncfusion/ej2-angular-navigations';
import { ActivityWorkflow, Workflow } from '../activity-workflow';
import { Query } from '@syncfusion/ej2-data/src';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';

@Component({
  selector: 'app-activity-workflow-form',
  templateUrl: './activity-workflow-form.component.html',
  styleUrls: ['./activity-workflow-form.component.css']
})
export class ActivityWorkflowFormComponent implements OnInit {
  public positions: any;
  @ViewChild('notif') notif: SharedToastComponent;
  public sysMessage: any = { show: false, content: '', cssStyle: '' };
  workflowForm: FormGroup;
  isUpdate = false;
  formarrayValidity = true;
  actions;
  public checked = false;
  activityId: number;
  public dropdownObj: any;
  public fields: object = { text: 'name', value: 'id' };
  public positionFields: object = { text: 'position', value: 'id' };
  public treeObj: TreeView;
  submitted: boolean;
  public filterType = 'StartsWith';
  public sorting = 'Ascending';
  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private workflowService: ActivityWorkflowService,
    private location: Location,
    private router: Router
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.activityId = +this.activatedRoute.snapshot.paramMap.get('id');
    if (this.activityId) {
      this.isUpdate = true;
      this.workflowService
        .getWorkflow(this.activityId)
        .subscribe((data: any) => {
          this.initializeForm(data);
          this.getPositions('');
        });
    }
    this.workflowService.getActions().subscribe((data: any) => {
      this.actions = data;
    });
  }
  createForm(): void {
    this.workflowForm = this.formBuilder.group({
      action: ['', Validators.required],
      minJobGrade: ['', Validators.required],
      maxJobGrade: ['', Validators.required],
      workFlowArray: this.formBuilder.array([this.createWorkFlowArray()])
    });
  }

  initializeForm(activity: ActivityWorkflow) {
    this.workflowForm = this.formBuilder.group({
      id: [activity.action_id],
      action: [activity.action_id, Validators.required],
      minJobGrade: [activity.job_grade_lower, Validators.required],
      maxJobGrade: [activity.job_grade_higher, Validators.required],
      workFlowArray: this.formBuilder.array([])
    });
    activity.workflow.forEach(element => {
      this.workFlowArray.controls.push(this.initializeWorkflowArray(element));
    });
  }

  initializeWorkflowArray(workflow: Workflow) {
    return this.formBuilder.group({
      id: [workflow.id, Validators.required],
      step: [workflow.step, Validators.required],
      authorizedBy: [workflow.authorized_by, Validators.required]
    });
  }

  createWorkFlowArray() {
    return this.formBuilder.group({
      step: ['', Validators.required],
      authorizedBy: ['', Validators.required]
    });
  }

  addWorkFlowArray(): void {
    this.workFlowArray.controls.push(this.createWorkFlowArray());
  }

  deleteWorkFlowArray(index) {
    const deletedControlId = this.workFlowArray.controls[index].value.id;
    if (deletedControlId) {
      const conf = confirm('Are you sure you want to delete this item?');
      if (conf) {
        this.workflowService
          .removeFlow(deletedControlId)
          .subscribe((data: any) => {
            this.notif.Show({
              title: 'Success',
              content: data.message,
              cssClass: 'e-toast-success'
            });
          });
      }
      this.workFlowArray.removeAt(index);
    } else {
      this.workFlowArray.removeAt(index);
    }
  }

  get workFlowArray(): FormArray {
    return this.workflowForm.get('workFlowArray') as FormArray;
  }
  get action(): FormControl {
    return this.workflowForm.get('action') as FormControl;
  }

  get minJobGrade(): FormControl {
    return this.workflowForm.get('minJobGrade') as FormControl;
  }

  get maxJobGrade(): FormControl {
    return this.workflowForm.get('maxJobGrade') as FormControl;
  }

  prepareFormData(): ActivityWorkflow {
    if (this.workFlowArray.controls.length > 0) {
      this.workFlowArray.controls.forEach(element => {
        this.formarrayValidity = element.valid;
      });
    }
    const activity = new ActivityWorkflow();
    if (this.workflowForm.valid && this.formarrayValidity) {
      if (this.isUpdate) {
        (activity.action_id = this.action.value),
          (activity.job_grade_lower = this.minJobGrade.value);
        activity.job_grade_higher = this.maxJobGrade.value;
        this.workFlowArray.controls.forEach(element => {
          activity.workflow.push({
            step: element.value.step,
            authorized_by: element.value.authorizedBy,
            id: element.value.id
          });
        });
      } else {
        (activity.action_id = this.action.value),
          (activity.job_grade_lower = this.minJobGrade.value),
          (activity.job_grade_higher = this.maxJobGrade.value);
        this.workFlowArray.controls.forEach(element => {
          activity.workflow.push({
            step: element.value.step,
            authorized_by: element.value.authorizedBy
          });
        });
      }
      return activity;
    } else {
      return null;
    }
  }

  onSubmit() {
    const payload = this.prepareFormData();
    this.submitted = true;
    if (payload) {
      if (this.activityId) {
        this.workflowService.update(payload).subscribe((response: any) => {
          if (response.status) {
            this.router.navigate(['/workspace/activity-workflow'], {
              state: {
                data: {
                  title: 'Success',
                  content: response.message,
                  cssClass: 'e-toast-success'
                }
              }
            });
          } else {
            this.sysMessage = {
              show: true,
              content: response.message,
              cssStyle: 'danger'
            };
          }
        });
      } else {
        this.workflowService.create(payload).subscribe((response: any) => {
          if (response.status) {
            this.router.navigate(['/workspace/activity-workflow'], {
              state: {
                data: {
                  title: 'Success',
                  content: response.message,
                  cssClass: 'e-toast-success'
                }
              }
            });
          } else {
            this.sysMessage = {
              show: true,
              content: response.message,
              cssStyle: 'danger'
            };
          }
        });
      }
    }
  }

  reportTo(index) {
    this.checked = !this.checked;
    if (this.checked) {
      this.workFlowArray.controls[index].get('authorizedBy').setValue(0);
    } else {
    }
  }

  onFilteringPosition(e: any) {
    let query = new Query();
    query =
      e.text != '' ? query.where('position', 'contains', e.text, true) : query;
    this.workflowService.getPositions(e.text).subscribe((data: any) => {
      e.updateData(data);
    });
  }

  getPositions(positionChar) {
    this.workflowService.getPositions(positionChar).subscribe((data: any) => {
      this.positions = data;
    });
  }

  cancel() {
    this.location.back();
  }
}
