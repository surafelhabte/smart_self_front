export class Activity {
  id?: number;
  action: string;
  minJobGrade: number;
  maxJobGrade: number;
}

export class Workflow {
  id?: number;
  step: number;
  authorized_by: string;
}

export class ActivityWorkflow {
  action_id: string;
  job_grade_lower: number;
  job_grade_higher: number;
  workflow: Workflow[] = [];
}
