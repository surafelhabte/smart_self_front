import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivityWorkflowService } from '../activity-workflow.service';
import { Router } from '@angular/router';
import { GroupSettingsModel } from '@syncfusion/ej2-angular-grids';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';

@Component({
  selector: 'app-activity-workflow-list',
  templateUrl: './activity-workflow-list.component.html',
  styleUrls: ['./activity-workflow-list.component.css']
})
export class ActivityWorkflowListComponent implements OnInit {
  public workflows = [];
  public columnsToDisplay: any[] = [];
  public groupOptions: GroupSettingsModel;
  @ViewChild('notif') notif: SharedToastComponent;
  public incomingCommand = { edit: true, delete: true };
  constructor(
    private apiService: ActivityWorkflowService,
    private router: Router
  ) {
    this.columnsToDisplay.push({
      field: 'name',
      headerText: 'Activity',
      textAlign: 'left',
      width: 80
    });
    this.columnsToDisplay.push({
      field: 'job_grade_lower',
      headerText: 'Min Job Grade',
      textAlign: 'left',
      width: 80
    });
    this.columnsToDisplay.push({
      field: 'job_grade_higher',
      headerText: 'Max Job Grade',
      textAlign: 'left',
      width: 80
    });
    this.columnsToDisplay.push({
      field: 'step',
      headerText: 'Step',
      textAlign: 'left',
      width: 80
    });
    this.columnsToDisplay.push({
      field: 'position',
      headerText: 'Authorized position',
      textAlign: 'left',
      width: 100
    });
  }

  ngOnInit() {
    this.apiService.getWorkflows().subscribe((data: any[]) => {
      this.workflows = data;
    });

    this.groupOptions = {
      disablePageWiseAggregates: false,
      showDropArea: true,
      showGroupedColumn: false,
      columns: ['name']
    };
  }

  addWorkflow() {
    this.router.navigate(['workspace/activity-workflow/create']);
  }

  edit($event) {
    this.router.navigate(['workspace/activity-workflow/' + $event.id + '/update']);
  }

  delete($event) {
    const conf = confirm('Are you sure you want to remove this step?');
    if (conf) {
      this.apiService.removeFlow($event.id).subscribe((response: any) => {
        if (response.status === true) {
          this.notif.Show({
            title: 'Success',
            content: response.message,
            cssClass: 'e-toast-success'
          });
          this.ngOnInit();
        } else {
          this.notif.Show({
            title: 'Error',
            content: response.message,
            cssClass: 'e-toast-danger'
          });
        }
      });
    }
  }
}
