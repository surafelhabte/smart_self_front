import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ActivityWorkflowService {
  private url = environment.baseUrl;

  constructor(private http: HttpClient) {}

  getWorkflows() {
    return this.http.get(this.url + 'workflow/workflow/gets');
  }

  getActions() {
    return this.http.get(this.url + 'action/action/gets');
  }

  getPositions(position: string) {
    return this.http.get(this.url + 'position/position/gets/' + position);
  }

  deleteActivityWorkflow(deletedId) {
    return this.http.delete(this.url + 'workflow/workflow/delete/' + deletedId);
  }

  removeFlow(deletedId) {
    return this.http.delete(this.url + 'workflow/workflow/remove/' + deletedId);
  }

  create(workflow: any): any {
    return this.http.post(this.url + 'workflow/workflow/create', workflow);
  }

  getWorkflow(id: any) {
    return this.http.get(this.url + 'workflow/workflow/get/' + id);
  }

  update(workflow: any) {
    return this.http.post(this.url + 'workflow/workflow/Update', workflow);
  }
}
