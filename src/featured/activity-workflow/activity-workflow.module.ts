import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ActivityWorkflowFormComponent } from './activity-workflow-form/activity-workflow-form.component';
import { ActivityWorkflowListComponent } from './activity-workflow-list/activity-workflow-list.component';
import { SharedModule } from 'src/shared/shared.module';

const routes: Routes = [
  {
    path: 'create',
    component: ActivityWorkflowFormComponent,
    data: { title: 'Create activity workflow' }
  },
  {
    path: ':id/update',
    component: ActivityWorkflowFormComponent,
    data: { title: 'Update activity workflow' }
  },
  {
    path: '',
    component: ActivityWorkflowListComponent,
    data: { title: 'Activity Workflows' }
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), SharedModule],
  declarations: [ActivityWorkflowListComponent, ActivityWorkflowFormComponent]
})
export class ActivityWorkflowModule {}
