/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SalaryAdvanceService } from './salary-advance.service';
import {
  HttpTestingController,
  HttpClientTestingModule
} from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

describe('Service: SalaryAdvance', () => {
  let lService: SalaryAdvanceService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule]
    });

    lService = TestBed.get(SalaryAdvanceService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('Should get and list requested salary ', () => {
    const loan = [
      {
        id: '1',
        amount_of_loan: 'number',
        reason_for_loan: 'string',
        amount_per_month: 'number',
        tobePaidWithin: 'number'
      }
    ];

    lService.getMyRequests({ empId: 1 }).subscribe((data: any) => {
      expect(data).toBe(loan);
    });
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');
    req.flush(loan);
    httpMock.verify();
  });

  it('Should create Salary advance', () => {
    const loan = [
      {
        id: '1',
        amount_of_loan: 'number',
        reason_for_loan: 'string',
        guaranters: [{ name: 'Naol worku' }]
      }
    ];
    lService.create(loan).subscribe((data: any) => {
      expect(data.length).toBeGreaterThan(0);
    });
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');
    req.flush(loan);
    httpMock.verify();
  });

  it('Should update Salary advance', () => {
    const loan = [
      {
        id: '1',
        amount_of_loan: 'number',
        reason_for_loan: 'string',
        guaranters: [{ name: 'Naol worku' }]
      }
    ];

    lService.update(loan).subscribe((data: any) => {
      expect(data[0].id).toBe('1');
    });
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');
    req.flush(loan);
    httpMock.verify();
  });

  it('Should delete salary advance', () => {
    lService.deleteLoan(12).subscribe((data: any) => {
      expect(data).toBe(12);
    });

    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('DELETE');

    req.flush(12);

    httpMock.verify();
  });
});
