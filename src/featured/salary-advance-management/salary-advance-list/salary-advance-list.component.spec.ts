import { SalaryAdvanceListComponent } from './salary-advance-list.component';
import {
  ComponentFixture,
  TestModuleMetadata,
  TestBed,
  async
} from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SharedModule } from 'src/shared/shared.module';
import { SalaryAdvanceService } from '../salary-advance.service';

describe('SalaryAdvanceListComponent', () => {
  let component: SalaryAdvanceListComponent;
  let fixture: ComponentFixture<SalaryAdvanceListComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [RouterTestingModule, HttpClientTestingModule, SharedModule],
      providers: [SalaryAdvanceService],
      declarations: [SalaryAdvanceListComponent]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(SalaryAdvanceListComponent);
    component = fixture.componentInstance;
    component.securityObject = {
      username: 'naol'
    };
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
