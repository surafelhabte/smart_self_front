import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';
import { SalaryAdvanceService } from '../salary-advance.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-salary-advance-list',
  templateUrl: './salary-advance-list.component.html',
  styleUrls: ['./salary-advance-list.component.css']
})
export class SalaryAdvanceListComponent implements OnInit {
  public salaryAdvances = [];
  public columnsToDisplay: any[] = [];
  @ViewChild('notif') notif: SharedToastComponent;
  public incomingCommand = { edit: true, delete: true };
  securityObject: any;
  constructor(
    private apiService: SalaryAdvanceService,
    private router: Router
  ) {
    this.columnsToDisplay.push({
      field: 'created_date',
      headerText: 'Requested on',
      textAlign: 'left',
      width: 120
    });
    this.columnsToDisplay.push({
      field: 'amount',
      headerText: 'Amount',
      textAlign: 'left',
      width: 90
    });
    this.columnsToDisplay.push({
      field: 'status',
      headerText: 'Status',
      textAlign: 'left',
      width: 50
    });

    if (localStorage.getItem('selfServiceAccessToken')) {
      this.securityObject = JSON.parse(
        localStorage.getItem('selfServiceAccessToken')
      );
    }
  }

  ngOnInit() {
    const empInfo = { action_id: 2, emp_id: this.securityObject.employee_id };
    this.apiService.getMyRequests(empInfo).subscribe((data: any[]) => {
      this.salaryAdvances = data;
    });
  }

  add() {
    this.router.navigate(['workspace/salary-advance/create']);
  }

  edit($event) {
    this.router.navigate(['workspace/salary-advance/' + $event.id + '/update']);
  }

  delete($event) {
    const conf = confirm('Are you sure you want to delete this item?');
    if (conf) {
      this.apiService.deleteLoan($event.id).subscribe((response: any) => {
        if (response.status === true) {
          this.notif.Show({
            title: 'Success',
            content: response.message,
            cssClass: 'e-toast-success'
          });
          this.ngOnInit();
        } else {
          this.notif.Show({
            title: 'Error',
            content: response.message,
            cssClass: 'e-toast-danger'
          });
        }
      });
    }
  }
}
