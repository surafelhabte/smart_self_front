import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SalaryAdvanceListComponent } from './salary-advance-list/salary-advance-list.component';
import { SalaryAdvanceFormComponent } from './salary-advance-form/salary-advance-form.component';
import { SharedModule } from 'src/shared/shared.module';

const routes: Routes = [
  {
    path: 'create',
    component: SalaryAdvanceFormComponent,
    data: { title: 'Create Salary Advance Request' }
  },
  {
    path: ':id/update',
    component: SalaryAdvanceFormComponent,
    data: { title: 'Update Salary Advance Request' }
  },
  {
    path: '',
    component: SalaryAdvanceListComponent,
    data: { title: 'Salary Advance Requests' }
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), SharedModule],
  declarations: [SalaryAdvanceFormComponent, SalaryAdvanceListComponent]
})
export class SalaryAdvanceManagementModule {}
