export class LoanModel {
  id?: string;
  amount: number;
  reason: string;
}

export class Guaranters {
  id?: number;
  emp_id: string;
}

export class LoanFormModel {
  id?: string;
  amount: number;
  reason: string;
  guarantor: Guaranters[] = [];
}

export class LoanRequestModel {
  action_id: number;
  requested_by: string;
  emp_id: string;
  data = new LoanFormModel();
}
