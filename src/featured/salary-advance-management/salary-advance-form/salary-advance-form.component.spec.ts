import {
  ComponentFixture,
  TestModuleMetadata,
  TestBed,
  async
} from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from 'src/shared/shared.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SalaryAdvanceFormComponent } from './salary-advance-form.component';
import { SalaryAdvanceService } from '../salary-advance.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('SalaryAdvanceFormComponent', () => {
  let component: SalaryAdvanceFormComponent;
  let fixture: ComponentFixture<SalaryAdvanceFormComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [
        SharedModule,
        ReactiveFormsModule,
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [SalaryAdvanceFormComponent],
      providers: [SalaryAdvanceService]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(SalaryAdvanceFormComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    component.securityObject = {
      username: 'naol'
    };
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('on submit', () => {
    it('Submit should be called', () => {
      const submitFun = spyOn(component, 'onSubmit');
      component.onSubmit();
      expect(submitFun).toHaveBeenCalled();
    });
  });

  describe('Requirment form', () => {
    it('Should be created', () => {
      const obj1 = {
        createForm: () => null
      };
      spyOn(obj1, 'createForm');
      obj1.createForm();
      expect(obj1.createForm).toBeTruthy();
    });

    it('Should be invalid when empty', () => {
      expect(component.loanForm.valid).toBeFalsy();
    });
    it('salary form fields validity sample for amount field', () => {
      const reqField = component.loanAmount;
      expect(reqField.valid).toBeFalsy();
      reqField.setValue('action');
      expect(reqField.valid).toBeTruthy();
    });
  });
});
