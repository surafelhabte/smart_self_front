import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormArray,
  FormControl
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SalaryAdvanceService } from '../salary-advance.service';
import { Location } from '@angular/common';
import { Guaranters, LoanRequestModel } from './loan';
import { FilteringEventArgs } from '@syncfusion/ej2-angular-dropdowns';
import { EmitType } from '@syncfusion/ej2-base';
import { Query } from '@syncfusion/ej2-data';
import { EmployeeService } from 'src/shared/service/employee.service';
import { RequestForOtherComponent } from 'src/shared/request-for-other/request-for-other.component';

@Component({
  selector: 'app-salary-advance-form',
  templateUrl: './salary-advance-form.component.html',
  styleUrls: ['./salary-advance-form.component.css']
})
export class SalaryAdvanceFormComponent implements OnInit {
  public guarantors;
  @ViewChild('notif') notif: SharedToastComponent;
  @ViewChild('reqforOther') reqforOther: RequestForOtherComponent;
  public sysMessage: any = { show: false, content: '', cssStyle: '' };
  loanForm: FormGroup;
  isUpdate = false;
  formarrayValidity = true;
  loanId: string;
  action_id = 2;
  public fields: any = { text: 'full_name', value: 'employee_id' };
  submitted: boolean;
  securityObject: any;
  public loanInfo: any;
  public allowedAmountError = false;
  public guaranter_cumulative_salary: any[] = [0,0,0];
  public guaranter_cumulative_result: any = true;
  public guaranter_exclude: any[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private apiService: SalaryAdvanceService,
    private location: Location,
    private router: Router,
    private empService: EmployeeService
  ) {
    this.createForm();
    if (localStorage.getItem('selfServiceAccessToken')) {
      this.securityObject = JSON.parse(
        localStorage.getItem('selfServiceAccessToken')
      );
    }
  }

  ngOnInit() {
    this.addArray();
    this.guaranter_exclude.push(this.securityObject.employee_id);
    this.loadLoanInfo(this.securityObject.employee_id);
    this.loanId = this.activatedRoute.snapshot.paramMap.get('id');
    if (this.loanId) {
      this.isUpdate = true;
      this.getGuarantors();
      this.apiService.getLoan(this.loanId).subscribe((data: any) => {
        this.initializeForm(data);
      });
    }
  }

  createForm(): void {
    this.loanForm = this.formBuilder.group({
      loanAmount: ['', Validators.required],
      reasonOfLoan: ['', Validators.required],
      guaranterArray: this.formBuilder.array([this.createGuaranterArray()])
    });
  }

  createGuaranterArray() {
    return this.formBuilder.group({
      guaranter: ['', Validators.required]
    });
  }

  initializeForm(loan) {
    this.loanForm = this.formBuilder.group({
      id: [loan.id],
      loanAmount: [loan.amount, Validators.required],
      reasonOfLoan: [loan.reason, Validators.required],
      guaranterArray: this.formBuilder.array([])
    });
    loan.guarantor.forEach(element => {
      this.guaranterArray.controls.push(this.initializeGuaranterArray(element));
    });

    if (loan.step > 1) {
      this.loanForm.disable();
    }
  }

  initializeGuaranterArray(emp: Guaranters) {
    return this.formBuilder.group({
      id: [emp.id, Validators.required],
      guaranter: [emp.emp_id, Validators.required]
    });
  }

  addArray(): void {
    this.guaranterArray.controls.push(this.createGuaranterArray());
    this.guaranterArray.controls.push(this.createGuaranterArray());
  }

  deleteArray(index) {
    const deletedControlId = this.guaranterArray.controls[index].value.id;
    if (deletedControlId) {
      const conf = confirm('Are you sure you want to delete this item?');
      if (conf) {
        this.apiService
          .removeGuaranter(deletedControlId)
          .subscribe((data: any) => {
            this.notif.Show({
              title: 'Success',
              content: data.message,
              cssClass: 'e-toast-success'
            });
          });
      }
      this.guaranterArray.removeAt(index);
    } else {
      this.guaranterArray.removeAt(index);
    }
  }

  get guaranterArray(): FormArray {
    return this.loanForm.get('guaranterArray') as FormArray;
  }

  get loanAmount(): FormControl {
    return this.loanForm.get('loanAmount') as FormControl;
  }

  get reasonOfLoan(): FormControl {
    return this.loanForm.get('reasonOfLoan') as FormControl;
  }

  prepareFormData(): LoanRequestModel {
    if (this.guaranterArray.controls.length > 0) {
      this.guaranterArray.controls.forEach(element => {
        this.formarrayValidity = element.valid;
      });
    }
    const request = new LoanRequestModel();
    if (
      this.reasonOfLoan.valid &&
      this.loanAmount.valid &&
      this.formarrayValidity
    ) {
      if (this.isUpdate) {
        request.data.id = this.loanId;
        request.data.amount = this.loanAmount.value;
        request.data.reason = this.reasonOfLoan.value;
        request.requested_by = this.securityObject.employee_id;
        request.emp_id =
          this.reqforOther.canRequestForOther && this.reqforOther.checked
            ? this.reqforOther.employeeReqId
            : this.securityObject.employee_id;
        this.guaranterArray.controls.forEach(element => {
          request.data.guarantor.push({
            emp_id: element.value.guaranter,
            id: element.value.id
          });
        });
      } else {
        request.action_id = this.action_id;
        request.requested_by = this.securityObject.employee_id;
        request.emp_id =
          this.reqforOther.canRequestForOther && this.reqforOther.checked
            ? this.reqforOther.employeeReqId
            : this.securityObject.employee_id;
        request.data.amount = this.loanAmount.value;
        request.data.reason = this.reasonOfLoan.value;
        this.guaranterArray.controls.forEach(element => {
          request.data.guarantor.push({
            emp_id: element.value.guaranter
          });
        });
    }
      return request;
    } else {
      return null;
    }
  }

  onSubmit() {
    const payload = this.prepareFormData();
    this.submitted = true;
    if (payload) {
      if (this.loanId) {
        this.apiService.update(payload).subscribe((response: any) => {
          if (response.status) {
            this.router.navigate(['/workspace/salary-advance'], {
              state: {
                data: {
                  title: 'Success',
                  content: response.message,
                  cssClass: 'e-toast-success'
                }
              }
            });
          } else {
            this.sysMessage = {
              show: true,
              content: response.message,
              cssStyle: 'danger'
            };
          }
        });
      } else {
        this.apiService.create(payload).subscribe((response: any) => {
          if (response.status) {
            this.router.navigate(['/workspace/salary-advance'], {
              state: {
                data: {
                  title: 'Success',
                  content: response.message,
                  cssClass: 'e-toast-success'
                }
              }
            });
          } else {
            this.notif.Show({
              title: 'Error',
              content: response.message,
              cssClass: 'e-toast-danger'
            });
          }
        });
      }
    }
  }

  cancel() {
    this.location.back();
  }

  public onFilteringGuarantors: EmitType<any> = (e: FilteringEventArgs) => {
    let query = new Query();
    query = e.text != '' ? query.where('full_name', 'contains', e.text, true) : query;

    const filterInfo = { keyword: e.text, emp_id: this.guaranter_exclude };

    this.apiService.getGuarantors(filterInfo).subscribe((data: any) => {
      e.updateData(data);
    });

  }

  loadLoanInfo(employeeId) {
    this.apiService
      .getLoanInfo({ emp_id: employeeId })
      .subscribe((data: any) => {
        this.loanInfo = data;
        !this.loanInfo.status ? this.loanForm.disable() : null;
      });
  }

  getGuarantors() {
    const filterInfo = { keyword: '', emp_id: this.securityObject.employee_id };

    this.apiService.getGuarantors(filterInfo).subscribe((data: any) => {
      this.guarantors = data;
    });
  }

  validateAmount(amount) {
    this.allowedAmountError = false;
    if (amount > this.loanInfo.allowed_credit) {
      this.loanAmount.setValue(0);
      this.allowedAmountError = true;
    }
  }

  public onSelect: EmitType<any> = (index,value) => {
    let total: any = 0;
    this.guaranter_cumulative_salary.splice(index, 1, +value.salary);
    this.guaranter_exclude.splice(index + 1, 1, value.employee_id);
    
    this.guaranter_cumulative_salary.forEach(element => {
      if(!isNaN(element)){
        total += element;
      }
    });

    if(total >= parseFloat(this.securityObject.salary)){
      this.guaranter_cumulative_result = true;

    } else {
      this.guaranter_cumulative_result = false;

    }

  }

  
}
