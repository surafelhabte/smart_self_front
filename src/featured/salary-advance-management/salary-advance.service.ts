import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SalaryAdvanceService {
  private url = environment.baseUrl;

  constructor(private http: HttpClient) {}

  getMyRequests(emp_info) {
    return this.http.post(this.url + 'loan/loan/filter', emp_info);
  }

  getGuarantors(filterInfo) {
    return this.http.post(this.url + 'loan/loan/getGuarantors', filterInfo);
  }

  deleteLoan(deletedId) {
    return this.http.delete(this.url + 'loan/loan/delete/' + deletedId);
  }

  removeGuaranter(deletedId) {
    return this.http.delete(this.url + 'loan/loan/remove/' + deletedId);
  }

  create(workflow: any): any {
    return this.http.post(this.url + 'loan/loan/create', workflow);
  }

  getLoan(id: any) {
    return this.http.get(this.url + 'loan/loan/get/' + id);
  }

  update(loan: any) {
    return this.http.post(this.url + 'loan/loan/Update', loan);
  }

  getLoanInfo(emp_id) {
    return this.http.post(this.url + 'loan/loan/GetLoanInfo', emp_id);
  }
}
