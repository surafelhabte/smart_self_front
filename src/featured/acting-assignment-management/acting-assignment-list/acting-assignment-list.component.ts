import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';
import { ActingAssignmentService } from '../acting-assignment.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-acting-assignment-list',
  templateUrl: './acting-assignment-list.component.html',
  styleUrls: ['./acting-assignment-list.component.css']
})
export class ActingAssignmentListComponent implements OnInit {
  @ViewChild('notif') notif: SharedToastComponent;

  public data: any = [];
  public columnsToDisplay: any[] = [];
  public incomingCommand = { edit: true, delete: true };
  public id: any;
  public securityObject: any;

  constructor(
    private service: ActingAssignmentService,
    private router: Router,
    private actRoute: ActivatedRoute
  ) {
    this.prepare();
    if (localStorage.getItem('selfServiceAccessToken')) {
      this.securityObject = JSON.parse(
        localStorage.getItem('selfServiceAccessToken')
      );
    }
  }

  ngOnInit() {
    this.id = this.actRoute.snapshot.params.id;
    this.service
      .filter({ emp_id: this.securityObject.employee_id, action_id: 8 })
      .subscribe((data: any) => {
        this.data = data;
      });
  }

  add() {
    this.router.navigate(['workspace/acting-assignment/create']);
  }

  edit($event) {
    this.router.navigate(['workspace/acting-assignment/' + $event.id + '/update']);
  }

  delete($event) {
    this.service.delete($event.id).subscribe((response: any) => {
      if (response.status === true) {
        this.notif.Show({
          title: 'Success',
          content: response.message,
          cssClass: 'e-toast-success'
        });
        this.ngOnInit();
      } else {
        this.notif.Show({
          title: 'Error',
          content: response.message,
          cssClass: 'e-toast-danger'
        });
      }
    });
  }

  prepare() {
    const col = [
      {
        field: 'proposed_employee',
        headerText: 'Proposed Employee'
      },
      {
        field: 'position',
        headerText: 'Position Tobe Filled'
      },
      {
        field: 'starting_date',
        headerText: 'Starting Date'
      },
      {
        field: 'completion_date',
        headerText: 'Completion Date'
      },
      {
        field: 'status',
        headerText: 'Status'
      },
      {
        field: 'created_date',
        headerText: 'create Date'
      }
    ];
    col.forEach(element => {
      this.columnsToDisplay.push({
        field: element.field,
        headerText: element.headerText,
        textAlign: 'center',
        width: 120
      });
    });
  }
}

