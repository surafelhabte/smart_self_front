import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ActingAssignmentFormComponent } from './acting-assignment-form/acting-assignment-form.component';
import { ActingAssignmentListComponent } from './acting-assignment-list/acting-assignment-list.component';
import { SharedModule } from 'src/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: ActingAssignmentListComponent,
    data: { title: 'Acting assignment requests' }
  },
  {
    path: 'create',
    component: ActingAssignmentFormComponent,
    data: { title: 'Create Acting assignment request' }
  },
  {
    path: ':id/update',
    component: ActingAssignmentFormComponent,
    data: { title: 'Update Acting assignment request' }
  }
];

@NgModule({
  imports: [CommonModule, SharedModule, RouterModule.forChild(routes)],
  declarations: [ActingAssignmentFormComponent, ActingAssignmentListComponent]
})
export class ActingAssignmentManagementModule {}
 