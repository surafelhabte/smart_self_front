import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ActingAssignmentService {
  private url = environment.baseUrl;

  constructor(private http?: HttpClient) { }

  create(request: any): any {
    return this.http.post(this.url + 'acting/acting/create', request);
  }

  filter(filter) {
    return this.http.post(this.url + 'acting/acting/filter', filter);
  }

  get(id: any) {
    return this.http.get(this.url + 'acting/acting/get/' + id);
  }

  update(request: any) {
    return this.http.post(this.url + 'acting/acting/update', request);
  }

  delete(id: any) {
    return this.http.delete(this.url + 'acting/acting/delete/' + id);
  }


  load_positions(emp_id) {
    return this.http.post(this.url + 'position/position/get', emp_id);
  }

  getEmployees(keyword) {
    return this.http.post(this.url + 'employee/employee/gets/', keyword);
  }


}
