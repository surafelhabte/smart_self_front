import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActingAssignmentService } from '../acting-assignment.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Query } from '@syncfusion/ej2-data/src';

@Component({
  selector: 'app-acting-assignment-form',
  templateUrl: './acting-assignment-form.component.html',
  styleUrls: ['./acting-assignment-form.component.css']
})
export class ActingAssignmentFormComponent implements OnInit {
  @ViewChild('notif') notif: SharedToastComponent;
  public formGroup: FormGroup;
  public formSubmitted = false;
  public employeeFields: any = { text: 'full_name', value: 'employee_id' };
  public positionFields: any = { text: 'position', value: 'id' };

  public ID: any = undefined;
  public isUpdate = false;

  public requestPlan: any[] = ['Recruitment', 'Transfer'];
  public employees: any[];
  public positions: any[];
  securityObject: any;

  constructor(
    private service: ActingAssignmentService,
    private fb?: FormBuilder,
    private router?: Router,
    private location?: Location,
    private actRoute?: ActivatedRoute
  ) {
    this.formGroup = this.fb.group({
      id: [''],
      position_tobe_filled: ['', Validators.required],
      reason_for_vacancy: ['', Validators.required],
      requesting_division: ['', Validators.required],
      proposed_emp_id: ['', Validators.required],
      starting_date: ['', Validators.required],
      completion_date: ['', Validators.required],
      replacement_plan: ['', Validators.required]
    });

    if (localStorage.getItem('selfServiceAccessToken')) {
      this.securityObject = JSON.parse(
        localStorage.getItem('selfServiceAccessToken')
      );
    }
  }

  ngOnInit() {
    this.load_positions();
    this.ID = this.actRoute.snapshot.params.id;
    if (this.ID !== undefined) {
      this.service.get(this.ID).subscribe((data: any) => {
        this.isUpdate = true;
        data.step > 1 ? this.formGroup.disable() : null;
        this.set_value(data);
      });
    }
  }

  Submit(): any {
    if (!this.formGroup.valid) {
      this.formSubmitted = true;
      return;
    } else {
      if (this.isUpdate) {
        return this.service
          .update({ data: JSON.stringify(this.formGroup.value) })
          .subscribe((response: any) => {
            if (response.status === true) {
              this.router.navigate(['/workspace/acting-assignment'], {
                state: {
                  data: {
                    title: 'Success',
                    content: response.message,
                    cssClass: 'e-toast-success'
                  }
                }
              });
            } else {
              this.notif.Show({
                title: 'Error',
                content: response.message,
                cssClass: 'e-toast-danger'
              });
            }
          });
      } else {
        return this.service
          .create({
            requested_by: this.securityObject.employee_id,
            emp_id: this.securityObject.employee_id,
            action_id: 8,
            data: JSON.stringify(this.formGroup.value)
          })
          .subscribe((response: any) => {
            if (response.status === true) {
              this.router.navigate(['/workspace/acting-assignment'], {
                state: {
                  data: {
                    title: 'Success',
                    content: response.message,
                    cssClass: 'e-toast-success'
                  }
                }
              });
            } else {
              this.notif.Show({
                title: 'Error',
                content: response.message,
                cssClass: 'e-toast-danger'
              });
            }
          });
      }
    }
  }

  cancel() {
    this.location.back();
  }

  load_positions() {
    this.service.load_positions({emp_id : this.securityObject.employee_id}).subscribe((data: any) => {
      this.positions = data;
    });
  }

  set_value(data) {
    this.formGroup.controls.id.setValue(data.id);
    this.formGroup.controls.position_tobe_filled.setValue(
      data.position_tobe_filled
    );
    this.formGroup.controls.reason_for_vacancy.setValue(
      data.reason_for_vacancy
    );
    this.formGroup.controls.requesting_division.setValue(
      data.requesting_division
    );
    this.formGroup.controls.proposed_emp_id.setValue(data.proposed_emp_id);
    this.formGroup.controls.starting_date.setValue(data.starting_date);
    this.formGroup.controls.completion_date.setValue(data.completion_date);
    this.formGroup.controls.replacement_plan.setValue(data.replacement_plan);
  }

  onFilteringEmployee(e: any) {
    let query = new Query();
    query =
      e.text !== '' ? query.where('full_name', 'contains', e.text, true) : query;
    this.service.getEmployees({keyword: e.text}).subscribe((data: any) => {
      e.updateData(data);
    });
  }
}
