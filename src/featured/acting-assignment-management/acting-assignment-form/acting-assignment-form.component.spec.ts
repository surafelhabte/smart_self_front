import {
  ComponentFixture,
  TestModuleMetadata,
  TestBed,
  async
} from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from 'src/shared/shared.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ActingAssignmentFormComponent } from './acting-assignment-form.component';
import { ActingAssignmentService } from '../acting-assignment.service';

describe('ActingAssignmentFormComponent', () => {
  let component: ActingAssignmentFormComponent;
  let fixture: ComponentFixture<ActingAssignmentFormComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [
        SharedModule,
        ReactiveFormsModule,
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [ActingAssignmentFormComponent],
      providers: [ActingAssignmentService]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(ActingAssignmentFormComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    component.securityObject = {
      employee_id: 'ECX/123'
    };
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('on submit', () => {
    it('Submit should be called', () => {
      const submitFun = spyOn(component, 'Submit');
      component.Submit();
      expect(submitFun).toHaveBeenCalled();
    });
  });

  describe('Acting assignment form', () => {
    it('Should be created', () => {
      const obj1 = {
        createForm: () => null
      };
      spyOn(obj1, 'createForm');
      obj1.createForm();
      expect(obj1.createForm).toBeTruthy();
    });

    it('Should be invalid when empty', () => {
      expect(component.formGroup.valid).toBeFalsy();
    });
    it('acting assingment form fields validity sample for family field', () => {
      const reqField = component.formGroup.controls.starting_date;
      expect(reqField.valid).toBeFalsy();
      reqField.setValue('family');
      expect(reqField.valid).toBeTruthy();
    });
  });
});
