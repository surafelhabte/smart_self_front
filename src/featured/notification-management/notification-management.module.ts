import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationDetailComponent } from './notification-detail/notification-detail.component';
import { NotificationListComponent } from './notification-list/notification-list.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: NotificationListComponent,
    data: { title: 'Notifications' }
  },
  {
    path: ':id/view',
    component: NotificationDetailComponent
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), SharedModule],
  declarations: [NotificationDetailComponent, NotificationListComponent],
  exports: [NotificationListComponent]
})
export class NotificationManagementModule {}
