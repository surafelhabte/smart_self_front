import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../notification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-notification-list',
  templateUrl: './notification-list.component.html',
  styleUrls: ['./notification-list.component.css']
})
export class NotificationListComponent implements OnInit {
  public notifications = [];
  securityObject: any;

  constructor(private apiService: NotificationService, private router: Router) {
    if (localStorage.getItem('selfServiceAccessToken')) {
      this.securityObject = JSON.parse(
        localStorage.getItem('selfServiceAccessToken')
      );
    }
  }

  ngOnInit() {
    const empId = { emp_id: this.securityObject.employee_id };
    this.apiService.getNotification(empId).subscribe((data: any) => {
      this.notifications = data;
    });
  }

  viewNotificationDetail(notificationId, notfStatus, notifName) {
    this.apiService
      .markAsRead({ request_id: notificationId, seen: 1 })
      .subscribe((data: any) => {
        this.apiService.notificationStatus = notfStatus;
        this.apiService.requestName = notifName;
        this.router.navigate(['workspace/notification/' + notificationId + '/view']);
      });

  }

  markAsRead(id) {
    this.apiService
      .markAsRead({ request_id: id, seen: 1 })
      .subscribe((data: any) => {});
  }
}
