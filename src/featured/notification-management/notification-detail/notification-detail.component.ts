import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../notification.service';
import { PositionDataModel } from '@syncfusion/ej2-angular-popups';
import { EmitType } from '@syncfusion/ej2-base';
import { Location } from '@angular/common';

@Component({
  selector: 'app-notification-detail',
  templateUrl: './notification-detail.component.html',
  styleUrls: ['./notification-detail.component.css']
})
export class NotificationDetailComponent implements OnInit {
  notfStatus: string;
  notfName: string;
  public position: PositionDataModel = { X: 'center', Y: 'center' };
  public closeOnEscape = false;
  public dialogCloseIcon = true;
  public defaultWidth = '452px';
  public target = '.control-section';
  public targetElement: HTMLElement;
  public dlgButtons: Object[] = [
    {
      click: this.dlgBtnClick.bind(this),
      buttonModel: { content: 'Yes', isPrimary: 'true' }
    },
    { click: this.dlgBtnClick.bind(this), buttonModel: { content: 'No' } }
  ];
  dlgBtnClick() {
    alert('yess');
  }
  constructor(
    private apiService: NotificationService,
    private location: Location
  ) {
    this.notfStatus = this.apiService.notificationStatus;
    this.notfName = this.apiService.requestName;
  }

  ngOnInit() {}

  public dialogClose: EmitType<object> = () => {
    this.location.back();
  }
}
