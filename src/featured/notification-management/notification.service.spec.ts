/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import {
  HttpTestingController,
  HttpClientTestingModule
} from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NotificationService } from './notification.service';

describe('Service: Request', () => {
  let nService: NotificationService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule]
    });

    nService = TestBed.get(NotificationService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('Should get and list notification ', () => {
    const notifs = [
      {
        id: '3fba4f70-5897-11ea-8f6d-8cec4ba694b1',
        name: 'For Employee',
        position: 'Manager',
        updated_date: '2020-02-26 16:01:10',
        created_date: '2020-02-26 15:55:22',
        status: 'approve'
      }
    ];

    nService.getNotification('EP123').subscribe((data: any) => {
      expect(data).toBe(notifs);
    });
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');
    req.flush(notifs);
    httpMock.verify();
  });
});
