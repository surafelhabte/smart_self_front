import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  public notificationStatus: string;
  public requestName: string;
  private url = environment.baseUrl;
  constructor(private http: HttpClient) {}

  getNotification(empId: any) {
    return this.http.post(this.url + 'notification/notification/get/', empId);
  }

  getAllNotification(empId: any) {
    return this.http.post(
      this.url + 'notification/notification/getAll/',
      empId
    );
  }

  markAsRead(viewed) {
    return this.http.post(
      this.url + 'notification/notification/update/',
      viewed
    );
  }
}
