import { TestBed } from '@angular/core/testing';
import {
  HttpTestingController,
  HttpClientTestingModule
} from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from 'src/shared/shared.module';
import { WorkExperienceService } from './work-experience.service';

describe('Service: WorkExprienceService', () => {
  let mService: WorkExperienceService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule, SharedModule]
    });

    mService = TestBed.get(WorkExperienceService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('Should get and list filtered requests ', () => {
    const request = [
      {
        id: '6139c456-5563-11ea-8b2e-8cec4ba694b1',
        name: 'Work Experience',
        status: null,
        step: '0',
        created_date: '2020-02-22 14:06:33'
      }
    ];

    mService.get({ action_id: 2, emp_id: 'EP123' }).subscribe((data: any) => {
      expect(data).toBe(request);
    });
    // telling the httmock what kind of request we expect and toward which url
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('GET');

    // fire the request with its data we really expect

    req.flush(request);

    httpMock.verify();
  });
  it('Should create work experience request', () => {
    const activity = [
      {
        action_id: '1',
        emp_id: 'EMP123',
        reason: 'asadas'
      }
    ];
    mService.create(activity).subscribe((data: any) => {
      expect(data.length).toBeGreaterThan(0);
    });
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');
    req.flush(activity);
    httpMock.verify();
  });

  it('Should get work experience request work flow successfull', () => {
    const request = {
      id: '6139c456-5563-11ea-8b2e-8cec4ba694b1',
      name: 'Work Experience',
      status: null,
      step: '0',
      created_date: '2020-02-22 14:06:33'
    };

    mService.get(request.id).subscribe((data: any) => {
      expect(data).toBe(request);
    });
    // telling the httmock what kind of request we expect and toward which url
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('GET');

    // fire the request with its data we really expect

    req.flush(request);

    httpMock.verify();
  });

  it('Should update experience ', () => {
    const request = {
      id: '6139c456-5563-11ea-8b2e-8cec4ba694b1',
      name: 'Work Experience',
      status: null,
      step: '0',
      created_date: '2020-02-22 14:06:33'
    };

    mService.update(request).subscribe((data: any) => {
      expect(data.step).toBe('0');
    });
    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('POST');

    req.flush(request);

    httpMock.verify();
  });

  it('Should delete job requirment', () => {
    mService.delete(12).subscribe((data: any) => {
      expect(data).toBe(12);
    });

    const req = httpMock.expectOne(request => {
      return true;
    });
    expect(req.request.method).toBe('DELETE');

    httpMock.verify();
  });
});
