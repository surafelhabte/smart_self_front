import {
  ComponentFixture,
  TestModuleMetadata,
  TestBed,
  async
} from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from 'src/shared/shared.module';
import { WorkExprienceListComponent } from './work-exprience-list.component';
import { WorkExperienceService } from '../work-experience.service';

describe('WorkExprienceListComponent', () => {
  let component: WorkExprienceListComponent;
  let fixture: ComponentFixture<WorkExprienceListComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [RouterTestingModule, HttpClientTestingModule, SharedModule],
      providers: [WorkExperienceService],
      declarations: [WorkExprienceListComponent]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(WorkExprienceListComponent);
    component = fixture.componentInstance;
    component.securityObject = {
      username: 'naol'
    };
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };

  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
