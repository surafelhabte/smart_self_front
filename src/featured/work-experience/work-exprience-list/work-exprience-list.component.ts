import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';
import { WorkExperienceService } from '../work-experience.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-work-exprience-list',
  templateUrl: './work-exprience-list.component.html',
  styleUrls: ['./work-exprience-list.component.css']
})
export class WorkExprienceListComponent implements OnInit {
  @ViewChild('notif') notif: SharedToastComponent;

  public data: any = [];
  public columnsToDisplay: any[] = [];
  public incomingCommand = { edit: true, delete: true };
  public id: any;
  securityObject: any;

  constructor(
    private service: WorkExperienceService,
    private router: Router,
    private actRoute: ActivatedRoute
  ) {
    this.prepare();
    if (localStorage.getItem('selfServiceAccessToken')) {
      this.securityObject = JSON.parse(
        localStorage.getItem('selfServiceAccessToken')
      );
    }
  }

  ngOnInit() {
    this.service
      .filter({ emp_id: this.securityObject.employee_id, action_id: '4' })
      .subscribe((data: any) => {
        this.data = data;
      });
  }

  add() {
    this.router.navigate(['workspace/work-experience/create']);
  }

  edit($event) {
    this.router.navigate(['workspace/work-experience/' + $event.id + '/update']);
  }

  delete($event) {
    const conf = confirm('Are you sure you want ot delete this request ?');
    if (conf) {
      this.service.delete($event.id).subscribe((response: any) => {
        if (response.status === true) {
          this.notif.Show({
            title: 'Success',
            content: response.message,
            cssClass: 'e-toast-success'
          });
          this.ngOnInit();
        } else {
          this.notif.Show({
            title: 'Error',
            content: response.message,
            cssClass: 'e-toast-danger'
          });
        }
      });
    } else {
      return null;
    }
  }

  prepare() {
    const col = [
      {
        field: 'name',
        headerText: 'Name'
      },
      {
        field: 'status',
        headerText: 'Status'
      },
      {
        field: 'created_date',
        headerText: 'Requested on'
      }
    ];
    col.forEach(element => {
      this.columnsToDisplay.push({
        field: element.field,
        headerText: element.headerText,
        textAlign: 'center',
        width: 120
      });
    });
  }
}
