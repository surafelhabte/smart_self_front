import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WorkExperienceService {

  private url = environment.baseUrl + 'experience/experience';

  constructor(private http?: HttpClient) { }

  create(request: any): any {
    return this.http.post(this.url + '/create', request);
  }

  filter(filter) {
    return this.http.post(this.url + '/filter', filter);
  }

  get(id: any) {
    return this.http.get(this.url + '/get/' + id);
  }

  update(request: any) {
    return this.http.post(this.url + '/update', request);
  }

  delete(id: any) {
    return this.http.delete(this.url + '/delete/' + id);
  }

}
