import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';
import { WorkExperienceService } from '../work-experience.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Location } from '@angular/common';
import { RequestForOtherComponent } from 'src/shared/request-for-other/request-for-other.component';

@Component({
  selector: 'app-work-experience',
  templateUrl: './work-experience.component.html',
  styleUrls: ['./work-experience.component.css']
})
export class WorkExperienceComponent implements OnInit {
  @ViewChild('notif') notif: SharedToastComponent;
  @ViewChild('reqforOther') reqforOther: RequestForOtherComponent;
  public formGroup: FormGroup;
  public formSubmitted = false;

  public ID: any = undefined;
  public isUpdate = false;

  public data: any[];
  securityObject: any;

  constructor(
    private service: WorkExperienceService,
    private fb?: FormBuilder,
    private router?: Router,
    private location?: Location,
    private actRoute?: ActivatedRoute
  ) {
    this.formGroup = this.fb.group({
      id: [''],
      reason: ['', Validators.required]
    });

    if (localStorage.getItem('selfServiceAccessToken')) {
      this.securityObject = JSON.parse(
        localStorage.getItem('selfServiceAccessToken')
      );
    }
  }

  ngOnInit() {
    this.ID = this.actRoute.snapshot.params.id;
    if (this.ID !== undefined) {
      this.isUpdate = true;
      this.service.get(this.ID).subscribe((data: any) => {
        data.step > 1 ? this.formGroup.disable() : null;
        delete data.step;
        this.inistializeForm(data);
      });
    }
  }

  inistializeForm(data) {
    this.formGroup = this.fb.group({
      id: [data.id],
      reason: [data.reason, Validators.required]
    });
  }

  Submit(): any {
    if (!this.formGroup.valid) {
      this.formSubmitted = true;
      return;
    } else {
      if (this.isUpdate) {
        return this.service
          .update({
            requested_by: this.securityObject.employee_id,
            emp_id:
              this.reqforOther.canRequestForOther && this.reqforOther.checked
                ? this.reqforOther.employeeReqId
                : this.securityObject.employee_id,
            data: JSON.stringify(this.formGroup.value)
          })
          .subscribe((response: any) => {
            if (response.status === true) {
              this.router.navigate(['/workspace/work-experience'], {
                state: {
                  data: {
                    title: 'Success',
                    content: response.message,
                    cssClass: 'e-toast-success'
                  }
                }
              });
            } else {
              this.notif.Show({
                title: 'Error',
                content: response.message,
                cssClass: 'e-toast-danger'
              });
            }
          });
      } else {
        return this.service
          .create({
            requested_by: this.securityObject.employee_id,
            emp_id:
              this.reqforOther.canRequestForOther && this.reqforOther.checked
                ? this.reqforOther.employeeReqId
                : this.securityObject.employee_id,
            action_id: '4',
            data: JSON.stringify(this.formGroup.value)
          })
          .subscribe((response: any) => {
            if (response.status === true) {
              this.router.navigate(['/workspace/work-experience'], {
                state: {
                  data: {
                    title: 'Success',
                    content: response.message,
                    cssClass: 'e-toast-success'
                  }
                }
              });
            } else {
              this.notif.Show({
                title: 'Error',
                content: response.message,
                cssClass: 'e-toast-danger'
              });
            }
          });
      }
    }
  }

  cancel() {
    this.location.back();
  }
}
