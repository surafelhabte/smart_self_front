import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkExperienceComponent } from './work-exprience-form/work-experience.component';
import { Routes, RouterModule } from '@angular/router';
import { WorkExprienceListComponent } from './work-exprience-list/work-exprience-list.component';
import { SharedModule } from 'src/shared/shared.module';

const routes: Routes = [
  {
    path: 'create',
    component: WorkExperienceComponent,
    data: { title: 'Create Work experience request' }
  },
  {
    path: ':id/update',
    component: WorkExperienceComponent,
    data: { title: 'Update Work experience request' }
  },
  {
    path: '',
    component: WorkExprienceListComponent,
    data: { title: 'Work experience requests' }
  }
];

@NgModule({
  imports: [CommonModule, SharedModule, RouterModule.forChild(routes)],
  declarations: [WorkExperienceComponent, WorkExprienceListComponent]
})
export class WorkExperienceModule {}
